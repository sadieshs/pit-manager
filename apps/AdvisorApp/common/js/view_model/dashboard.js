App.viewModels.dashboard=function(rootScope,actionProcessor,localeProcessor){
	var self=this;
	self.showLogin = true;
	self.langSelector = function(lang, langupdated){
		
		console.log('lang',lang)
		//localeProcessor.setLocaleForUser(lang,true);
		//rootScope.$apply();
		rootScope.setLocalizedLiteralsForLocale(lang);
		rootScope.appLiterals = actionProcessor.getLocalizedLiterals();
		setTimeout(function() {
			console.log('yo yo');
			if(langupdated){
				self.langSelector(lang);
				rootScope.$apply();
			}
		}, 300);
		
	}
	self.displayLogin = function(){
		self.showLogin = false;
	}
	/*self.displayLogin = function(){
		if(self.username === "admin" && self.password ==="melco@123"){
           self.showLogin = false;
		}
		else{
			self.username ="";
			self.password = "";
			alert("Please Enter correct username and Password");
		}
	} */

	self.data={"responsesList":[{"pageData":{"alert":[{"customerName":"Henry","message":"Call Henry to confirm appointment","time":"3:30","type":"newCustomer"}],"stock":[{"stockName":"BSE Sensex","price":"26,525.46","change":"-200.88","pChange":"-0.75","icon":"down","color":"red"},{"stockName":"Gold","price":"1,306.90","change":"+18.60","pChange":"+1.44","icon":"up","color":"green"},{"stockName":"Oil","price":"46.29","change":"-1.72","pChange":"-3.58","icon":"down","color":"red"},{"stockName":"USDINR","price":"67.3264","change":"+0.245","pChange":"+0.37","icon":"up","color":"green"}],"recentNews":[{"img":"images/news1.jpg","titel":"Invest 1 lakh and earn 72 lakh "},{"img":"images/news2.jpg","titel":"Launches educational policy"},{"img":"images/news3.jpg","titel":"Plan your retirement"}]},"notification":{"message":["Customer Smith is ready to buy the product offered","Offer is rejected by Mohan","Anil has some query about the product","Please contact Rohan"]},"myLeads":[{"customerName":"Hardi Pithwa","convinced":"77","email":"hardi@gmail.com","timePeriod":"07"},{"customerName":"Shashank Patil","convinced":"69","email":"shashank@gmail.com","timePeriod":"04"},{"customerName":"Kushal","convinced":"30","email":"kushal@gmail.com","timePeriod":"08"},{"customerName":"Ritesh","convinced":"21","email":"ritesh@gmail.com","timePeriod":"02"}],"chatBot":{"customers":[{"customerName":"Ram","interest":"New Products","availability":"online"},{"customerName":"Mohan","interest":"Follow up","availability":"offline"},{"customerName":"Rohan","interest":"Customer history","availability":"online"},{"customerName":"Gita","interest":"Leader board","availability":"offline"},{"customerName":"Smith","interest":"Lorem Ipsum","availability":"online"},{"customerName":"Hardi","interest":"Appointments","availability":"offline"}]},"agentDetail":{"agent":[{"agentName":"Raj kumar","contactNo":"9008986543","address":"#21,floor 3,2nd Cross,Jainagar","pin":"560123"},{"agentName":"Samandeep","contactNo":"9008986543","address":"#21,floor 3,2nd Cross,Jainagar","pin":"560123"},{"agentName":"Ravi krishnan","contactNo":"9008986543","address":"#21,floor 3,2nd Cross,Jainagar","pin":"560123"}]}}],"isSuccessful":"true"};
	self.getDashoardResponse=function(response){
		x1=self;
		self.data=response||self.data;
		// self.pageData=self.data.responsesList[0].pageData;
		// self.myLeads=self.data.responsesList[0].myLeads;
		// self.chatBot=self.data.responsesList[0].chatBot;
		// self.agentDetail=self.data.responsesList[0].agentDetail;
		// console.log(self.pageData,self.myLeads,"chatbot:", self.chatBot, self.agentDetail)
	}
	self.today=new Date().getDate();
}