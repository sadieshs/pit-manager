App.viewModels.model=function(){
	var self=this;
	m = self;
	// declare variables here
	self.searchString='';
	self.event={
		eventName:'',
		eventDate:Date.now(),
		eventType:'',
		budget:0,
		noOfPeople:0,
		contactVendor:false
	}
	self.eventTypeOptions=["Press Conference", "Birthday"];
	self.uiDetails={};
	self.templateData={};
	self.templateId='';
	self.allEventsList=[];
	self.selectedEvent={};
	self.payload={};
	self.selectedCheckList={};
	//self.selectedTask={};
	self.selectVendorType='VENUE';
	self.selectedVendor={};
	self.allVendors=[];
	self.fromVendorSelection = false;
	self.newCheckListName ='';
	self.newCheckListTag = '';
	
	self.successCallback = function(){
		console.log("In success callback");
	}
	self.errorCallback = function(){
		console.log('Error callback');
	}
	self.initNewEventPage = function(){
		self.event.eventDate = new Date().toLocaleDateString();
	}
	self.initCheckListPage=function(response){
		if(!self.fromVendorSelection){
			self.event.eventDate = new Date().toLocaleDateString();//moment().format('DD MM YYYY');
			//self.uiDetails = response.data[0].ui;
			self.templateData = response.data[0];
			self.templateId = response.data[0]._id;
		}
		else{
			self.selectedCheckList.vendorName = self.selectedVendor.name;
			self.selectedCheckList.vendorId = self.selectedVendor._id;
		}
	};
	self.initAllEventsPage=function(response){
		self.allEventsList = response.data;
	}
	self.initVendorSelectionPage = function(response){
		self.allVendors = response.data;
	}
	self.preparePayload=function(){
		self.payload = self.templateData;
		self.payload.eventName = self.event.eventName;
		self.payload.noOfPeople = self.event.noOfPeople;
		self.payload.budget = self.event.budget;
		self.payload.canVendorContact = false; //self.event.contactVendor;
		self.payload.eventDate = new Date(self.event.eventDate);
		self.payload.templateId = self.templateId;
		//self.payload.checkList = self.templateData.checkList;
		console.log(self.payload);
	}
	self.vendorSelectionClicked = function(checkList){
		self.selectVendorType = checkList.tags[0];	
		self.selectedCheckList = checkList;
		//return "";
	}
	self.onAddCheckListClick = function(){
		// var tags = [];
		// tags.push(self.newCheckListTag)
		var newChkList = {
			checkListName: self.newCheckListName,
			status:'',
			vendorName:'',
			tags: [self.newCheckListTag]
		}
		self.templateData.checkList.push(newChkList);
	}
}