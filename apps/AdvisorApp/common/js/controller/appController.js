AppController.controller("AppController", ['$scope', '$rootScope', '$http', '$route', '$location', '$q', '$timeout',
                                           'ActionProcessor','LocaleProcessor','UIControlsService', 'UIStubsService', 'Logger',
                                           function($scope, $rootScope, $http, $route, $location, $q, $timeout,
                                        		   ActionProcessor, LocaleProcessor, UIControlsService, UIStubsService, Logger) {

     	var self = $scope;
	   self.tab = "Dashboard";
	   self.setTab = function(newTab){
		self.tab = newTab;
		$location.path('/'+newTab);
	}
   	self.isSet = function(tabValue){
		return self.tab === tabValue;
	}
	 $scope.hamFun = function(){
	     $(document).ready(function(){
		    $('#burger').click(function(){
			    $(this).toggleClass('open');
			    $(this).toggleClass('hclose');
		    });
	     });
	 
	     $scope.isHidePitStatusTable = true; 
	     $scope.isHidePitDetailTable = true;
	     $scope.tableNoEntered=function(){
	         if ($("#tableInput").val().length > 0) {
	             $("#pitDrp").prop("disabled", true);
	             $("#areaDrp").prop("disabled", true);
	         }
	         if ($("#tableInput").val().length == 0) {
	             $("#pitDrp").prop("disabled", false);
	             $("#areaDrp").prop("disabled", false);
	             $scope.isHidePitDetailTable = true;
	         }
	     
	         if ($("#tableInput").val().length == 3) {
	             $scope.isHidePitDetailTable = false;
	         }
	     
	     }
	     $scope.drpPitSelected = function () {
	         $("#tableInput").prop("disabled", true);
	     }

	     $scope.drpAreaSelected = function(){
	         $scope.isHidePitStatusTable = false;
	     }

	     $scope.onPitStatusRowClick = function () {
	         $scope.isHidePitStatusTable = true;
	         $scope.isHidePitDetailTable = false;
	     }
	     $scope.resetPage = function () {
	         $scope.isHidePitStatusTable = true;
	         $scope.isHidePitDetailTable = true;
	     }

        $scope.mapFun = function(){
	        $("#test").swipe( {
                    //Generic swipe handler for all directions
                    swipe:function(event, direction, distance, duration, fingerCount, fingerData) {
                        $('.first-half').toggleClass('swip');
              
                    },
                    //Default is 75px, set to 0 for demo so any distance triggers swipe
                        threshold:0
                    });
        }
         /* $(".hclose").click(function(){
              $('.right-menu').css('right','0');
          });*/
          setTimeout(function(){
      	    $(".dashboard-wrapper").click(function(){
              $('.right-menu').css('right','-26%');
		
      	    });
          }, 1000);
          setTimeout(function(){
          $("#burger.open").click(function(){
              $('.right-menu').css('right','-26%');
            });
          }, 500);
    }

	// Initialize method. This method is called when the app launches
	$scope.initializeApp = function() {
		$scope.successResponse = ActionProcessor.getSuccessResponse();
		$scope.errorResponse = ActionProcessor.getErrorResponse();
		Logger.fatal("Success and error callback registered !" + $scope.successResponse);
		if (ActionProcessor.isAppInitialized() == true) {
			// This method gets called every time a new page is routed to.
			// We need this check without which we end up in an infinite loop with
			// each page launch triggering an app launch
			return;
		}

		//initialize all viewmodels here
		$rootScope.isStubbedVersion = true;
		//console.log("appConfig=",appConfig);
		$rootScope.WIDTH = $(window).width();
		$rootScope.HEIGHT = $(window).height();
x2=$rootScope;
		$rootScope.dashboard=new App.viewModels.dashboard($rootScope, ActionProcessor, LocaleProcessor);
		$rootScope.model = new App.viewModels.model();
		$rootScope.prelogin = new App.viewModels.prelogin();
		$rootScope.creditRequest = new App.viewModels.creditRequest();
		$rootScope.customers = new App.viewModels.customers();
		$rootScope.postRating = new App.viewModels.postRating();
		$rootScope.adjustGamingTable = new App.viewModels.adjustGamingTable();
		
		// Flag to show and hide the Alert messages
		$rootScope.showAlertFlag = false;
		$rootScope.selectVendorClick=function(checkList){
			//debugger;
			$rootScope.model.vendorSelectionClicked(checkList);
		}
		$rootScope.dismissModal = function(){
			$('myModal').modal('hide');
		}
		// Flag to show and hide the loading screen for maps
		$rootScope.loadMapFlag = false;

		$rootScope.showAlert = function(msg){
			UIControlsService.showSuccessOverlayScreen(msg);
			//$rootScope.$apply();
		}
		
		$rootScope.showErrorPopup = function(msg, okText){
			UIControlsService.showAlertOverlayScreen(msg, okText);
			//$rootScope.$apply();
		}
		
		$rootScope.hideAlert = function(msg){
			UIControlsService.hideAlertOverlayScreen();
			//$rootScope.$apply();
		}
		$rootScope.hideValidationAlert = function() {
			UIControlsService.hideValidationAlert();
		}

		// A flag to trac if the user is logged in or not?
		if (angular.isUndefined($rootScope.isUserLoggedIn)){
			$rootScope.isUserLoggedIn = false;
		}

		ActionProcessor.init()
			.then(
				function(payload) {
					Logger.fatal("App framework initialized successfully");
					// AuthenticationEventHandler.init($scope);
					// Now that the framework is initialized, let us cache
					// the localized string literals
					$rootScope.appLiterals = ActionProcessor.getLocalizedLiterals();
					Logger.fatal("The string constants is " + JSON.stringify($rootScope.appLiterals));
				}, 
				function(errorPayload) {
					Logger.fatal("Error initializing app framework");
				}
			);		
	},

	// The method invoked by partial HTML templates when they are
	// changing states

	/**
	 * It will call set global event 
	 * @constructor
	 * @param {string} eventName- event name.
	 * @param {number} timeout- timeout in order to call event after some time (optional).
	 * @param {object} options- options (optional).
	 */
	$scope.setGlobalEvent = function(eventName, timeout, options){

		//UIControlsService.toggleHideMenu(); 

		// This is a hack. If the event was 'onErrorPageOKClick', then ActionProcessor
		// does not return a promise.
		if (eventName == "onErrorPageOKClick") {
			ActionProcessor.setGlobalEvent(eventName,timeout, options);
			Logger.fatal("Global State machine completed successfully with onErrorPageOKClick");
		} else {

			if(timeout){
				$location.path('/navigation/common/resources/FinacleLoaderPage');
				$rootScope.$apply();
				setTimeout(function(){
					ActionProcessor.setGlobalEvent(eventName, timeout,options).then(function(payload){
						Logger.fatal("Global State machine completed successfully : "+ payload);
						$scope.successResponse = payload.responsesList;// payload.data;
					});
				},timeout);
			}else{
				ActionProcessor.setGlobalEvent(eventName, timeout,options).then(function(payload){
					Logger.fatal("Global State machine completed successfully : "+ payload);
					$scope.successResponse = payload.responsesList;// payload.data;
				});
			}
		}
	};

	$scope.setEvent = function(eventName, isGlobal) {
		if(eventName instanceof Array){
			//Handle array based setEvent flow
		}
		// Just delegate this event call to the ActionProcessor
		$scope.successResponse = {}; $scope.errorResponse = {};

		var tPromise = ActionProcessor.setEvent(eventName, isGlobal);
		
		tPromise.
		then(function(payload) {
			$scope.successResponse = payload.responsesList;// payload.data;
			Logger.fatal("State machine completed successfully : "+ JSON.stringify(payload));
		}, function(errorPayload) {
			$scope.errorResponse = errorPayload;
			Logger.fatal("Error running the state machine");
		});
	};

	// Utility method that returns an array of options which can be used by fin-input
	// for type "dropdown"
	$scope.createDropDownOptionArray = function(dataArr, keyStr, valueStr, beginStrArr, endStrArr) {
		return UIControlsService.createDropDownOptionArray(dataArr, keyStr, valueStr, beginStrArr, endStrArr);
	}
	$scope.toggleSidebar = function() {
        $scope.toggle = !$scope.toggle;
        $cookieStore.put('toggle', $scope.toggle);
    };
    $scope.toggleShowMenu = function() {
        if(_.isUndefined($scope.showMenu)){
            $scope.showMenu = false;
        } 

        $scope.showMenu = !$scope.showMenu;  

        var margin = $("#fms-content").css("margin-left"); 

        if (margin == "0px") {
            $("#fms-content").css("margin-left", "-28em");
            $(".finacle-container").css({"position": "relative", "width": "100%"});
            $(".absolute").css({"position": "fixed", "width": "100%"});
        } else {
            $("#fms-content").css("margin-left", "0"); 
            $(".finacle-container").css({"position": "fixed", "width": "100%"});
            $(".absolute").css({"position": "absolute", "width": "100%"});
        } 

    };

$scope.responsiveHeight=function(){
	var fullHeight = $rootScope.HEIGHT; //window.innerHeight()
	 var chatHeight = fullHeight-200;
     $('.video-cont').css('height',chatHeight+'px');
}


    // calender code starts here

var ctrl = $scope.ctrl = this;
  
  this.months = 'January February March April May June July August September October November December'.split(' ');
  this.day = new Date();
  this.preDays = [];
  this.days = [];
  this.postDays = [];
  this.headings = [''];
  this.monthName = '';
  this.back = false;
  this.locked = false;
  this.lockTime = 500 + 350 * 2;

   $scope.$watch('ctrl.day', function () { ctrl.updateDate(ctrl); });

  this.next = function () {
    if (this.locked) return;
    this.locked = true;
    $timeout(function () { this.locked = false; }.bind(this), this.lockTime);
    this.day.setMonth(this.day.getMonth() + 1);
    this.back = false;
    $element.removeClass('back');
    this.updateDate();
  };
  this.prev = function () {
    if (this.locked) return;
    this.locked = true;
    $timeout(function () { this.locked = false; }.bind(this), this.lockTime);
    this.day.setMonth(this.day.getMonth() - 1);
    this.back = true;
    $element.addClass('back');
    this.updateDate();
  };
  this.updateDate = function (day) {
    this.day.setDate(1);
    this.preDays = this.getDays(this.day.getDay(), 0);
    this.days = this.getDays(this.getDaysInMonth(), this.preDays.length);
    this.postDays = this.getDays(
      (7 - (this.preDays.length + this.days.length) % 7) % 7,
      this.preDays.length + this.days.length
    );
    this.monthName = this.months[this.day.getMonth()];
    this.headings = [{ month: this.monthName, year: this.day.getFullYear() }];
  };
  this.setDelay = function (item) {
    var left = item.prop('offsetLeft') - $element.prop('offsetLeft'),
        top = item.prop('offsetTop') - $element.prop('offsetTop'),
        dist = Math.sqrt(left * left + top * top),
        delay = dist * 0.75;
    item.css('transition-delay', delay + 'ms');
  };
  this.getDays = function (count, start) {
    var days = [], i;
    for (i = start; i < count + start; i++) {
      days.push({ col: i % 7, row: Math.floor(i / 7) });
    }
    return days;
  };
  this.getDaysInMonth = function () {
    switch (this.day.getMonth()) {
      case 1: return new Date(this.day.getFullYear(), 1, 29).getMonth() == 1 ? 29 : 28;
      case 3: case 5: case 8: case 10: return 30;
      default: return 31;
    }
  };

    // end of vcalender code
	// Initialize this app
	$scope.initializeApp();

	  var vm = this;

    // The model object that we reference
    // on the  element in index.html
    // vm.rental = {};
    
    // // An array of our form fields with configuration
    // // and options set. We make reference to this in
    // // the 'fields' attribute on the  element
    // vm.rentalFields = [
    //     {
    //         key: 'first_name',
    //         type: 'input',
    //         className: 'floatRight',
    //         templateOptions: {
    //             type: 'text',
    //             label: 'First Name',
    //             placeholder: 'Enter your first name',
    //             required: true
    //         }
    //     },
    //     {
    //         key: 'last_name',
    //         type: 'input',
    //         templateOptions: {
    //             type: 'text',
    //             label: 'Last Name',
    //             placeholder: 'Enter your last name',
    //             required: true
    //         }
    //     },
    //     {
    //         key: 'email',
    //         type: 'input',
    //         templateOptions: {
    //             type: 'email',
    //             label: 'Email address',
    //             placeholder: 'Enter email',
    //             required: true
    //         }
    //     },
    //      {
    //         key: 'email',
    //         type: 'input',
    //         templateOptions: {
    //             type: 'email',
    //             label: 'Mani',
    //             placeholder: 'Enter email',
    //             required: true
    //         }
    //     },
	// 	 {
    //         key: 'email',
    //         type: 'input',
    //         templateOptions: {
    //             type: 'email',
    //             label: 'Mani',
    //             placeholder: 'Enter Mail ID',
    //             required: true
    //         }
    //     }
    // ];

}]);
