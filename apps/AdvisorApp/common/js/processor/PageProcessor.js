var AppController = angular.module("AppController");
AppController.factory('PageProcessor', ['$http', '$q', 'Logger',
                               function($http, $q, Logger) {
	// variables that will be used across this service
	var pagesConfig = {};
	
	// function definitions
	return {
		
		// The init method which initializes processing of all pages configurations
		init: function() {
			
			// Note that we will have to return a promise because the caller
			// is waiting on us to complete some functionality
			var deferred = $q.defer();
			// Let us read the file now. This is the TemplateProcessor
		    $http.get('navigation/pagesConfig.json').success(function(data) {
		    	// The only thing to do is to process the pagesconfig and do any initialization
		    	// that is required.
		    	pagesConfig = data;
		    	Logger.debug("Finished processing pages configuration");
		    	deferred.resolve(data);
		    }).error(function(msg, code) {
		    	deferred.reject(msg);
		    });
		    
		    // Let us return the promise now
		    return deferred.promise;
		},
		// The method which fetches the url path for a given page
		getLocationUrlForPage: function(page) {
			return pagesConfig[page].templateUrl;
		}
	};
}]);