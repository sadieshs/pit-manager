/**
 * Service Inputs module to have object/data collection based on event names rather than adding additional
 * layer of configuration.
 * 
 * Arguments:
 * arguments & callback is optional in almost all the events below, only used when there is need 
 * for asynchronous input collection sequence in place. 
 */

var serviceInputs = {
		"MainController":function(args,callback){
			return {'input':'Main controller inputs'};
		},
		"ActionController":function(args,callback){
			return {'input':'action controller inputs'};
		},
		"UserAccountsService":function(args,callback){
			return {'input':'user accounts service'};
		},
		"RootAuthenticator":function(args,callback){
			return {'input':'rootAuth'};
		},
		"MBDemoController":function(args,callback){
			return  {
					LoginBtn:'Login',
					CancelPage:'HomePage.xml',
					USER_TYPE:'1',
					AUTHENTICATION_REQUEST:'true',
					helpPage:'Thin_SignOnRetRq',
					__JS_ENCRYPT_KEY__:'10001,8906e9fbaee94568ed7eb04d896454069114bb81509b300821b27d91c67824cb9ac6cfe9d6d371b13bde3ea09c2c1abda6a7c8dab1283c74e4afb895effc318625893b5740d3ecb6b43a1654651e2c8237bab1d906a7a60200d3d2cb007e49a803ae6c85376793e28ba08282f06d91a8661ad2dc1d44f46240e5ae9cabf25c65,131',
					JavaScriptEnabled:'N',
					deviceID:'',
					machineFingerPrint:'',
					deviceType:'',
					browserType:'',
					mbparam:'hBGrtz9ijA%2BnZOod1vuZoD9IVRscmlkBW%2BIDqUG6eoZjiIvVFBjV%2Fv5U',
					unique:'disabled',
					imc_service_page:'SignOnRetRq',
					Alignment:'LEFT',
					page:'SignOnRetRq',
					serviceType:'Dynamic',
					__AUTHENTICATE__:'1',
					CorpId:'FMBRET40',
					PassWord:'d#demo'
			}
		},
		"AuthenticationService":function(args,callback){
			return {CorpId:'BRET5',
				PassWord:'d#demo'};
		}
};

var challengeHandler = {
		"userformauth":function(args,callback){
			WL.Logger.fatal("User form auth invoked : ");
			var data = authRealmForm.respondToChallenge();
			WL.Logger.fatal("Challenge response is : "+ JSON.stringify(data));
		    formAuthRealmChallengeHandler.submitLoginForm(data.reqURL, data.options, formAuthRealmChallengeHandler.submitLoginFormCallback);
		    return;
		},
		"userpinauth":function(args,callback){
			var data = authRealmMPin.respondToChallenge();
			formAuthRealmChallengeHandler.submitLoginForm(data.reqURL, data.options, formAuthRealmChallengeHandler.submitLoginFormCallback);
			return;
		}
};

/*
 * The input for navigationInputs are events in the json file, however this can be evaluated 
 */
var navigationInputs = {
		"onCapchaCheckEvent":function(args,callback){
			return {'input':'catcha event'};
		}
};

var InputProcessor = {
	init:function(){
		//service manager - init if required
	},
	getInputsForType:function(action,actionType){
		switch(actionType){
		case "service":
			//Service invocation
			return this.getServiceInputs(action);
			break;
		case "challenge":
			//Challenge Invocation
			return this.getChallengeHandlerInputs(action);
			break;
		case "challengeresponse":
			//Challenge Response Invocation
			return this.getChallengeResponseHandler(action);
			break;
		case "navigation":
			//Navigation invocation
			return this.getNavigationInputs(action);
			break;
			default:
				WL.Logger.fatal("This condition should never be printed ! ServiceInputs.js");
				break;
		}
	},
	getServiceInputs:function(serviceName){
		return serviceInputs[serviceName];
	},
	getChallengeHandlerInputs:function(challengeName){
//		AuthenticationEventHandler.invokeChallengeHandler(challengeName);
	},
	getChallengeResponseHandler:function(challengeName){
//		WL.Logger.fatal("getChallengeResponseHandler : " + challengeName);
//		AuthenticationEventHandler.invokeChallengeHandler(challengeName);
//		WL.Logger.fatal("*************getChallengeResponseHandler*************");
	},
	getNavigationInputs:function(navigationName){
		return navigationInputs[navigationName];
	}
};
