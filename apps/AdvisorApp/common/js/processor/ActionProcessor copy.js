
AppController.factory('ActionProcessor', ['$http', '$rootScope', '$q', '$location', '$timeout', 'TemplateProcessor', 'ValidationService','UIControlsService', 'UIStubsService', 
                                          function($http, $rootScope, $q, $location, $timeout, TemplateProcessor, ValidationService,UIControlsService, UIStubsService) {

	// variables that will be used across this service
	//var featuresList;
	var appInitialized = false;
	var currentPage = '';
	var currentSubFeature = '';
	var currentSuccessResponse = {};
	var currentErrorResponse = {};
	var parameters='';
	//var invokedFromLoginFlow = false;
	//var encryptionEnabled = false;
	//var isMbaasConnected = true;


	var getModelParameters = function(eventInfoParams,currentActionType){
		var mbaasParams = {}; var sensitiveKeys = [];
		var parameters = []; var paramValue = '';
		// Let us pass the parameters a required for the service
		for (var paramKey in eventInfoParams.parameters) {
			// put yr encryption coc=de here
			// end of encryption part
			paramValue = $rootScope.$eval(eventInfoParams.parameters[paramKey]);
			mbaasParams[paramKey] = paramValue;
		}
		parameters.push(JSON.stringify(mbaasParams));
		//console.log("cm====>",parameters);
		return parameters;
	};


	/*
	 * getNextActionSequenceObject will return the object for next course of action decisions.
	 */
	var getNextActionSequenceObject = function(eventInfo){
		var nextPath = eventInfo.success;
		var nextSubFeatureName = '';
		var nextPageName = '';
		var successCallback = '';
		var pageUrl ='';
		// Check if the next page is within the same workflow
		// or a different one
		if (nextPath.hasOwnProperty('subFeature')) {
			// OK, we are moving to a new sub-feature
			nextSubFeatureName = nextPath.subFeature;
		} else {
			// We are going to be in the same subFeature.
			nextSubFeatureName = currentSubFeature;
		}
		if(nextPath.hasOwnProperty('callback')){
			successCallback = nextPath.callback;
		}
		nextPageName = nextPath.pageName;
		pageUrl = TemplateProcessor.getUrlForPage(nextPageName);	 
		return {
			"nextPageName":nextPageName,
			"nextSubFeatureName":nextSubFeatureName,
			"pageUrl":pageUrl,
			"callback":successCallback
		};
	};
	// function definitions
	return {

		// The init method which initializes the templates framework.
		// This function uses TemplateProcessor to process templates.
		init: function() {

			// Note that we will have to return a promise because the caller
			// is waiting on us to complete this functionality
			var deferred = $q.defer();
			//Open the encrypted cache
//			EncryptedCacheService.openCache();

			// Three things to do here.
			// Step 1. Initialize the TemplateProcessor
			// Step 2. Find out the launcher page
			// Step 3. Launch the first page

			// Let us initialize TemplateProcessor now
			TemplateProcessor.init().then(
					function(payload) {
						//WL.Logger.fatal("TemplateProcessor's init completed successfully");
						appInitialized = true;
						
						// Step 2. Let us now find out the launcher page
						var featureStart = TemplateProcessor.getFeatureStart();
						currentSubFeature = window.subFeature|| featureStart.sub_feature;
						var notiResponse = window.noti;
						//WL.Logger.fatal("The subfeature to start is " + featureStart);
						var pageName = window.pageName|| TemplateProcessor.getStartPageForSubFeature(currentSubFeature);
						window.subFeature=window.pageName=null;
						
						// $.ajax({url:"/currentUser",type:'get',async:false,
						// success:function(currentUser){
						// 	 if(currentUser){
						// 			  console.log('success called');
						// 			  currentSubFeature='dashboard';
						// 			  pageName='home';
        				// 			  $rootScope.user=currentUser;  
						// 		  }
						// }
						// });
						currentPage = pageName;
						var launchPage = TemplateProcessor.getUrlForPage(pageName);
						//WL.Logger.fatal("The start page is " + pageName + " with url " + launchPage);
						// Step 3. Let us now launch the page
						$location.path(launchPage);
						// Notify the one who is waiting on this promise
						deferred.resolve(payload);
					}, function(errorPayload) {
						// Logger.fatal("Error processing templates");
						deferred.reject(errorPayload);
					});

			// Let us return the promise now
			return deferred.promise;
		},
		// A method that tracks if the app was initialized
		isAppInitialized: function() {
			return appInitialized;
		},
		isUserLoggedIn:function(){
			return true;
			// This snippet is used to controll the isUserLoggedIn during stubbed mode
			// if($rootScope.isStubbedVersion){
			// 	return $rootScope.isUserLoggedIn;
			// }
			// // This is returned in a normal mode
			// return singleStepAuthRealmChallengeHandler.isUserLoggedIn("");
		},

		setEvent: function(eventName, isGlobal ) {
			var deferred = $q.defer();
			// We should not reset the cached responses.
			// Finacle mobile app will reuse cached responses.
			//currentSuccessResponse = {}; currentErrorResponse = {};
			// WL.Logger.fatal("The event called was " + eventName + " in " + currentPage);
			if(isGlobal){
				currentSubFeature='global';
				currentPage='noCurrentPage';
			}
			var parameters='';
			var payloadData;
			var workflow = TemplateProcessor.getWorkflowForSubFeature(currentSubFeature);
			// Let us identify the service to be invoked for this event
			var allPages = workflow.pageNavigation;
			// First, find the current page
			var index;
			for (index = 0; index < allPages.length; index++) {
				var thisPage = allPages[index];
				if (thisPage.hasOwnProperty(currentPage)) {
					// We found the page.
					break;
				}
			}
			// By the time we are here, we have found the page. Just in case, we have not, then it is
			// a serious exception
			if (index >= allPages.length) {
				// WL.Logger.fatal("Serious error. A page that was present in the configs has gone missing:" + currentPage);
				deferred.error({error: 'Internal error'});
				return;
			}
			// We found the page. Let us find the details of the event now
			var eventsIndex = 0;
			var currentPageEventsList = (allPages[index])[currentPage];
			// Let us now find the event and the corresponding action
			for (; eventsIndex < currentPageEventsList.length; eventsIndex++) {
				if (currentPageEventsList[eventsIndex].eventName == eventName) {
					// We found the event
					break;
				}
			}
			// We have found the event by the time we are here
			// Let us invoke the service
			var currentEventInfo = currentPageEventsList[eventsIndex];
			//Perform validations on the form if the validations is enabled.
			var formValid = true;

			// Check if the value is not defined. If so, then set it false by default.
			if (_.isUndefined(currentEventInfo.validation)) {
				currentEventInfo.validation = false;
			}

			if(currentEventInfo.validation == 'true'){
				// WL.Logger.info("Validations enabled for page: " + currentPage);
				var validationParams = currentEventInfo.validationParams;
				formValid = ValidationService.performValidation(validationParams);

				// If there are errors, then check if a popup is required. If so, display it
				if(!formValid){

					var keyArr = Object.keys($rootScope.pageErrorCode);
					for (var i = 0; i < keyArr.length; i++) {

						for (var j = 0; j < currentEventInfo.validationErrorMessage.length; j++) {
							var key = Object.keys(currentEventInfo.validationErrorMessage[j])[0];
							if ($rootScope.pageErrorCode[keyArr[i]] == key) {
								UIControlsService.showValidationAlert(currentEventInfo.validationErrorMessage[j][key]);

								deferred.reject();
							}
						}
					}
				}
			}

			// If there is no validation error, then proceed. 
			if(formValid){
				// if(currentEventInfo.hasOwnProperty('encryptionEnabled')){
				// 	encryptionEnable = currentEventInfo.encryptionEnabled;
				// }
				// else{
				// 	encryptionEnable = false;
				// }

				//var service = currentEventInfo.action;
				var actionType = currentEventInfo.actionType;

				if(actionType == 'navigation'){
					var nextActionObject = getNextActionSequenceObject(currentEventInfo);
					// We have all we need to move to the net page
					$location.path(nextActionObject.pageUrl);
					// Now that we have moved, let us remember which page we are in
					currentSubFeature = nextActionObject.nextSubFeatureName;
					currentPage = nextActionObject.nextPageName;
					deferred.resolve();
					return;
				}
				/**
				 * If we are here, the type of action is service or serviceNoNavigation
				 * Sequence for handling service invocation is detailed below.
				 * The following execution is for workflows with type : service in the JSON configs.
				 * If the actiontype is "service", the following sequence of steps of will be
				 * executed.
				 * 1. Redirect the user to "loader" page
				 * 2. Invoke the service and check the response
				 * 3. If the service invocation was successful, take the user to the next page.
				 * 4. If the service invocation is an error, take the user to the error page (if error page is not
				 * defined, then take the user to the default error page)
				 */

				// Step 1: Let us execute the first step where the loading screen is displayed.
				//Commented 
				// if(actionType != "serviceNoNavigation"){
				// 		$location.path('/navigation/common/resources/FinacleLoaderPage');	
				// 	}

				// Step 2: Invoke the service
				var basepath = window.location.protocol+'//'+window.location.host+'/';//"http://localhost:3000/"
				var uri = basepath + currentEventInfo.action;
				if(currentEventInfo.hasOwnProperty('element')){
					console.log($rootScope.$eval(currentEventInfo.element));
					uri = uri + '/' + $rootScope.$eval(currentEventInfo.element);
				}
				parameters = getModelParameters(currentEventInfo,actionType);
				if(currentEventInfo.hasOwnProperty('payload')){
					payloadData = $rootScope.$eval(currentEventInfo.payload);
				}else{
					payloadData=parameters;
				}
				uri = encodeURI(uri);
				var request = {
							method: currentEventInfo.httpMethod,
							url: uri,
							headers: {"Content-Type": "application/json;charset=UTF-8"},
							data: payloadData
				};
				$http(request).then(function(response){
					console.log(response);
					var nextActionObject = getNextActionSequenceObject(currentEventInfo);
					currentSuccessResponse = response;
					if(nextActionObject.callback != ''){
						var callback = nextActionObject.callback + '()';
						$rootScope.$eval(callback);
					}
					
					//service with notification
					if(actionType=="serviceNoNavNotify"){
						// call the toastr
						notificationService(response);
						return;
					}
					else if(actionType=="serviceNotify"){
						// call the toastr
						notificationService(response);
						// put the redirection code
						$location.path(nextActionObject.pageUrl);
						currentSubFeature = nextActionObject.nextSubFeatureName;
						currentPage = nextActionObject.nextPageName;
					}
					// reload entire site with new content
					if(actionType=="serviceWithRedirectUrl"){
						window.location.href=currentEventInfo.successPage.redirectTo;
					}
					if(actionType != "serviceNoNavigation"){
						$location.path(nextActionObject.pageUrl);
					// We have all we need to move to the next page
					// Now that we have moved, let us remember which page we are in
						currentSubFeature = nextActionObject.nextSubFeatureName;
						currentPage = nextActionObject.nextPageName;
						return deferred.resolve(response);
					}
					deferred.resolve(response);
				},function(error){
					console.log(error);
					currentErrorResponse = error;
					var errorPath = currentEventInfo.error;
					var pageUrl = TemplateProcessor.getUrlForPage(errorPath.pageName);
					if(errorPath.callback != ''){
						var callback = nextActionObject.callback + '()';
						$rootScope.$eval(callback);
					}
					if(actionType != "serviceNoNavigation"){
						$location.path(pageUrl);
						currentPage = errorPath.pageName;
					}
					deferred.reject(error);
					return;
				});
			}
			return deferred.promise;
		},
		getSuccessResponse:function(){
			return currentSuccessResponse;
		},
		getErrorResponse:function(){
			return currentErrorResponse;
		},
		// Method that fetches the entire literals mapping
		getLocalizedLiterals: function() {
			return TemplateProcessor.getLocalizedLiterals();
		}
	};
}]);
