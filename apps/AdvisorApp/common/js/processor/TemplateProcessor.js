var AppController = angular.module("AppController");
AppController.factory('TemplateProcessor', ['$http', '$q','FormProcessor','FeatureProcessor', 'PageProcessor', 'ValidationProcessor', 'LocaleProcessor','$rootScope',
                               function($http, $q,FormProcessor, FeatureProcessor, PageProcessor, ValidationProcessor, LocaleProcessor,$rootScope) {
	// variables that will be used across this service
	var appConfig = {};
	
	// function definitions
	return {
		
		// The init method which initializes processing of all configurations
		// and templates. This function uses FeatureProcessor to process
		// templates.
		init: function() {
			
			// Note that we will have to return a promise because the caller
			// is waiting on us to complete some functionality
			var deferred = $q.defer();
			// Let us read the file now. This is the TemplateProcessor
		    $http.get('mobileAppConfig.json').success(function(data) {
		    	// The first thing is to process the appconfig and do any initialization
		    	// that is required.
		    	appConfig = data;
		    	$rootScope.appVersionDetails = data;
		FormProcessor.init().then(function(formData) {			
		    	// The next step is to initialize the template processing logic
		    FeatureProcessor.init().then(function(featuresData) {
		    		// If we are here everything was successful
		    		// WL.Logger.debug("Feature processing from templates completed successfully");
		    		// Let us initialize the page processing logic as well
			    	PageProcessor.init().then(function(pageData) {
			    		// If we are here everything was successful
			    		// WL.Logger.debug("Processed pages configuration successfully");
			    		//Lets initialize the validation processor now;
			    		ValidationProcessor.init().then(function(validationData){
			    			//WL.Logger.debug("Validation config loaded successfully");
			    			LocaleProcessor.init(appConfig).then(function(localeData) {
			    				//WL.Logger.debug("Locale configuration processed successfully");
			    				deferred.resolve(localeData);
			    			}, function(errorLocaleData) {
			    				//WL.Logger.debug("Error processing locale configs");
			    				deferred.resolve(errorLocaleData);
			    			});
			    		}, function(errorValidationData){
			    			//WL.Logger.fatal("Error in loading validations config");
			    			deferred.reject(errorValidationData);
			    		});
						
			    	}, function(errorPageData) {
			    		// Lets log an error and notify the caller
			    		// WL.Logger.debug("Error processing pages configuration");
			    		deferred.reject(errorPageData);
			    	});
		    		
		    	}, function(errorFeaturesData) {
		    		// Lets log an error and notify the caller
		    		// WL.Logger.debug("Error processing all template features");
		    		deferred.reject(errorFeaturesData);
		    	});
		},function(errorFormData) {
		    		// Lets log an error and notify the caller
		    		// WL.Logger.debug("Error processing all template features");
		    		deferred.reject(errorFormData);
		    	});
		    }).error(function(msg, code) {
		    	deferred.reject(msg);
		    });
		    
		    // Let us return the promise now
		    return deferred.promise;
		},
		// The method which identifies the feature which should start the app
		getFeatureStart: function() {
			var featureStart = appConfig.featureStart;
			return featureStart;
		},
		// The method which fetches the start page for a given feature
		getStartPageForSubFeature: function(subFeature) {
			// This is something that is controlled by FeatureProcessor and 
			// PageProcessor.
			// Let us delegate
			var pageName = FeatureProcessor.getStartPageForSubFeature(subFeature);
			return pageName;
		},
		// The method which fetches the url for a given page
		getUrlForPage: function(page) {
			// This is something that is controlled by PageProcessor.
			// Let us delegate
			return PageProcessor.getLocationUrlForPage(page);
		},
		// The method which fetches the url for a given page
		getWorkflowForSubFeature: function(subFeature) {
			// This is something that is controlled by FeatureProcessor.
			// Let us delegate
			return FeatureProcessor.getWorkflowForSubFeature(subFeature);
		},
		// update language
		setLocalizedLiteralsForLocale: function(lang){
			return LocaleProcessor.setLocaleForUser(lang);
		},
		// The method which fetches the localized strings data
		getLocalizedLiterals: function() {
			return LocaleProcessor.getLocalizedLiterals();
		}
	};
}]);