
/* JavaScript content from js/processors/ValidationService.js in folder common */

/* JavaScript content from js/processors/ValidationService.js in folder common */
var AppController = angular.module("AppController");
AppController.factory('ValidationService', ['$http', '$rootScope', 'ValidationProcessor',
                               function($http, $rootScope, ValidationProcessor) {
	
	var requiredValidator = function(param){
		var fieldValue = $rootScope.$eval(param);
		var flag = true; 
		if(fieldValue == null || fieldValue.length == 0){
			$rootScope.pageErrorArr[param] = "emptyField";
			flag = false; 

			// $rootScope.$eval(param).errorFlag = true;
			// $rootScope.$eval(param).errorType = ;
			// validationError ? true : true;
		}

		return flag;
	};

	var conditionalRequiredValidator = function(param, conditionVar) {

		var fieldValue = $rootScope.$eval(conditionVar);

		if (fieldValue == null || fieldValue == "null" || fieldValue == true || fieldValue.length == 0) {
			return requiredValidator(param);
		} 

		return true;
	};
	
	var conditionalRequiredValidatorForForcePasword = function(param, conditionVar) {

		var fieldValue = $rootScope.$eval(conditionVar);

		if ( fieldValue == true ) {
			return requiredValidator(param);
		} 

		return true;
	};
	
	var conditionalRequiredValidatorForTrueCondition = function(param, conditionVar) {

		var fieldValue = $rootScope.$eval(conditionVar);

		if ( fieldValue == true ) {
			return requiredValidator(param);
		} 

		return true;
	};
	
	var maxLengthValidator = function(param, maxLength){
		var fieldValue = $rootScope.$eval(param);
		var flag = true; 

		if(fieldValue.length > maxLength){
			$rootScope.pageErrorArr[param] = "maxLength";
			flag = false; 

			// $rootScope.$eval(param).errorFlag = true;
			// $rootScope.$eval(param).errorType = "maxLength";
			// validationError = true;
		}
		return flag;

	};
	
	var minLengthValidator = function(param, minLength){
		var fieldValue = $rootScope.$eval(param);
		var flag = true; 

		if(fieldValue.length < minLength){
			$rootScope.pageErrorArr[param] = "minLength";
			flag = false; 
			// $rootScope.$eval(param).errorFlag = true;
			//$rootScope.$eval(param).errorType = "minLength";
			//validationError = true;
		}

		return flag;
	};

	var stringEqualsValidator = function(param, definedValue){
		
		var fieldValue = $rootScope.$eval(param);
		var definedValue = $rootScope.$eval(definedValue);
		
		var flag = true; 
			
		if(fieldValue != definedValue){
			$rootScope.pageErrorArr[param] = "stringNotEqual";
			flag = false; 
		}
		return flag;
	};
	
	var notAllCapsValidator = function(param) {
		var fieldValue = $rootScope.$eval(param);
		var flag = true; 
		if(fieldValue.length > 0 && fieldValue === fieldValue.toUpperCase()){
			$rootScope.pageErrorArr[param] = "notAllCaps";
			flag = false; 
		}

		return flag;
	};
	
	var actualDateLessThanValidator = function(param, definedValue){
		var fromDate = $rootScope.$eval(param);
		var flag = true; 
		toDate = getStringifiedDate(definedValue);
		if(fromDate > toDate){
			$rootScope.pageErrorArr[param] = "actualDateLessThan";
			flag = false; 
		}
		return flag;
		
	};
	
	var actualDateGreaterThanValidator = function(param, definedValue){
		var fromDate = $rootScope.$eval(param);
		var flag = true; 
		toDate = getStringifiedDate(definedValue);
		if(fromDate > toDate){
			$rootScope.pageErrorArr[param] = "actualDateGreaterThan";
			flag = false; 
		}
		return flag;
	};
	
	var dateLessThanValidator = function(param, definedValue){
		var fieldValue = $rootScope.$eval(param);
		var flag = true; 
		fieldValue = getStringifiedDate(fieldValue);
		definedValue = getStringifiedDate(definedValue);
		if(fieldValue > definedValue){
			$rootScope.pageErrorArr[param] = "dateLessThan";
			flag = false; 
		}
		return flag;
	};
	
	var dateGreaterThanValidator = function(param, definedValue){
		var fieldValue = $rootScope.$eval(param);
		var flag = true; 
		fieldValue = getStringifiedDate(fieldValue);
		definedValue = getStringifiedDate(definedValue);
		if(fieldValue < definedValue){
			$rootScope.pageErrorArr[param] = "dateGreaterThan";
			flag = false; 
		}
		return flag;
	};
	
	var dateLessThanVarValidator = function(param, variable){
		var fieldValue = $rootScope.$eval(param);
        var flag = true;
                                            
        if(fieldValue == null || fieldValue.length == 0){
            $rootScope.pageErrorArr[param] = "emptyField";
            flag = false;
        } else {
            var variableFieldValue = $rootScope.$eval(variable);
            if(variableFieldValue == null || variableFieldValue.length == 0){
              $rootScope.pageErrorArr[variable] = "emptyField";
              flag = false;
            } else {
                if(getStringifiedDate(fieldValue)  > getStringifiedDate(variableFieldValue)){
                    $rootScope.pageErrorArr[param] = "dateLessThanVar";
                    flag = false;
                }
            }
        }

		return flag;
	};
	
	var dateGreaterThanVarValidator = function(param, variable){
                                            
		var fieldValue = $rootScope.$eval(param);
        var flag = true;
                                            
        if(fieldValue == null || fieldValue.length == 0){
           $rootScope.pageErrorArr[param] = "emptyField";
           flag = false;
        } else {
            var variableFieldValue = $rootScope.$eval(variable);
            if(variableFieldValue == null || variableFieldValue.length == 0){
                $rootScope.pageErrorArr[variable] = "emptyField";
                flag = false;
            } else {
                variableFieldValue = getStringifiedDate(variableFieldValue);
                if(getStringifiedDate(fieldValue) < variableFieldValue){
                    $rootScope.pageErrorArr[param] = "dateGreaterThanVar";
                    flag = false;
                }
            }
        }
                                            
		return flag;
	};
	
	var conditionalDateGreaterThanVarValidator = function(param, conditionVar, variable) {

		var fieldValue = $rootScope.$eval(conditionVar);

		if (fieldValue == null || fieldValue == "null" || fieldValue.length == 0) {
			return dateGreaterThanVarValidator(param, variable);
		} 

		return true;
	};
	
	var dateGreaterThanNowValidator = function(param){
		var currDate = new Date();
		var currentDate = currDate.getFullYear().toString() + "-" + (currDate.getMonth() + 1).toString() + "-" + currDate.getDate().toString();
		var fieldValue = $rootScope.$eval(param);
		var flag = true; 
		fieldValue = getStringifiedDate(fieldValue);
		var definedValue = getStringifiedDate(definedValue);
		if(fieldValue <= definedValue){
			$rootScope.pageErrorArr[param] = "dateGreaterThanNow";
			flag = false; 
		}
		return flag;
	};
	
	var lessThanOptionalValidator = function(param, variable){
		var fieldValue = $rootScope.$eval(param);
		var flag = true; 
		if(fieldValue != null || fieldValue.length != 0){
			var variableFieldValue = $rootScope.$eval(variable);
			if(variableFieldValue != null || variableFieldValue.length != 0)
				if(parseFloat(variableFieldValue) < parseFloat(fieldValue)){
            		$rootScope.pageErrorArr[param] = "lessThanOptional";
        			flag = false;
				}
		}
		return flag;
	};
	var getStringifiedDate = function(value){
		var data = (value.toString()).split("-");
		var returnData = parseInt(data[0]) * 365 + parseInt(data[1]) * 30 + parseInt(data[2]);
		return returnData;
	};                                            
    var isEmailValidator = function(value){
    	var fieldValue = $rootScope.$eval(value);
		var flag = true;
		var regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	    flag = regex.test(fieldValue);
	    if(!flag){
	    	$rootScope.pageErrorArr[value] = "Invalid Email";
	    }
	    return flag;
    }
    
    var isPasswordValidator = function(value){
    	var fieldValue = $rootScope.$eval(value);
		var flag = true;
		var regex = /^[A-Za-z0-9]{8,}$/; 
	    flag = regex.test(fieldValue);
	    if(!flag){
	    	$rootScope.pageErrorArr[value] = "Invalid Password";
	    	flag = false;
	    }
	    return flag;
    }
    
	var lengthEqualsValidator = function(param, variable){
		var fieldValue = $rootScope.$eval(param);
		var flag = true; 
		fieldValue += "";
		if(fieldValue.length != variable){
			$rootScope.pageErrorArr[param] = "lengthNotEquals";
			flag = false;
		}
		return flag;
	};
	
	var notificationValidator=function(param){
		var fieldValue = $rootScope.$eval(param);
		for (var index = 0; index < fieldValue.length; index++) {
			if(fieldValue.notificationChannel == "email"){
				//Call email validator
			}
			if(fieldValue.notificationChannel == "sms"){
				//call mobileno validtor
			}
		}
	};
	
	var requiredIfPresentInArrayValidator=function(param,variable){
		var fieldValue = $rootScope.$eval(param);
		var count=$rootScope.$eval(variable);
		for(var index=0; index < count; index++) {
			if(fieldValue[index] == null || fieldValue[index].length == 0){
				var appendedParam=param+"["+index+"]";
				$rootScope.pageErrorArr[param] = "emptyField";
				flag = false; 
			}

			return flag;
		}
	}

	return{
		performValidation:function(parameters){
			//$q is used to return validation results to ActionProcessor
			//var deferred = $q.defer();
			
			//1. Iterate through the fields to be validated
			//2. Get the corresponding validations from the validation processor
			//3. Iterate through the validations 
			//4. Pass each validation to the corresponding validator.

			$rootScope.pageErrorArr = [];
			$rootScope.pageErrorCode = [];

			var validationError = true;

			for (var i = 0; i < parameters.length; i++) {
				
				var validations = ValidationProcessor.getValidationsForField(parameters[i]);

				for (var j = 0; j < validations.length; j++) {

					var res = true;
					switch(validations[j].validationType){
						case "required":
							res = requiredValidator(parameters[i]);
							validationError =validationError && res;
							break;
						
						case "conditionalRequired":
							res = conditionalRequiredValidator(parameters[i], validations[j].conditionVar);
							validationError =validationError && res;
							break;
							
						case "conditionalRequiredForForcePassword":
							res = conditionalRequiredValidatorForForcePasword(parameters[i], validations[j].conditionVar);
							validationError =validationError && res;
							break;
							
						case "conditionalRequiredValidatorForTrueCondition":
							res = conditionalRequiredValidatorForTrueCondition(parameters[i], validations[j].conditionVar);
							validationError =validationError && res;
							break;
							
						case "maxLength":
							res = maxLengthValidator(parameters[i], validations[j].maxLengthValue);
							validationError = validationError && res;
							break;
						
						case "minLength":
							res = minLengthValidator(parameters[i], validations[j].minLengthValue);
							validationError = validationError && res;
							break;
						
						case "notAllCaps":
							res = notAllCapsValidator(parameters[i]);
							validationError = validationError && res;	
							break;													
						
						case "dateLessThan":
							res = dateLessThanValidator(parameters[i], validations[j].dateLessThan);
							validationError = validationError && res;
							break;
						
						case "dateGreaterThan":
							res = dateGreaterThanValidator(parameters[i], validations[j].dateGreaterThan);
							validationError = validationError && res;
							break;
						
						case "dateLessThanVar":
							res = dateLessThanVarValidator(parameters[i], validations[j].dateLessThanVar);
							validationError = validationError && res;
							break;
						
						case "dateGreaterThanVar":
							res = dateGreaterThanVarValidator(parameters[i],validations[j].dateGreaterThanVar);
							validationError = validationError && res;
							break;

						case "conditionalDateGreaterThanVar":
							res = conditionalDateGreaterThanVarValidator(parameters[i], validations[j].conditionVar, validations[j].dateGreaterThanVar);
							validationError = validationError && res;
							break;
						
						case "dateGreaterThanNow":
							res = dateGreaterThanNowValidator(parameters[i]);
							validationError = validationError && res;
							break;

						case "lessThanOptional":
							res = lessThanOptionalValidator(parameters[i], validations[j].lessThanOptional);
							validationError = validationError && res;
							break;
                        
                        case "isEmail":
	                        res = isEmailValidator(parameters[i]);
	                        validationError = validationError && res;
	                        break;
                        case "isPassword":
	                        res = isPasswordValidator(parameters[i]);
	                        validationError = validationError && res;
	                        break;
                        case "lengthEquals":
	                        res = lengthEqualsValidator(parameters[i],validations[j].length);
	                        validationError = validationError && res;
	                        break;
                        
                        case "stringEquals":
	                        res = stringEqualsValidator(parameters[i],validations[j].newPassword);
	                        validationError = validationError && res;
	                        break;

                        case "actualDateGreaterThan":
	                        var res =actualDateGreaterThanValidator(parameters[i],validations[j].dateGreaterThanVar);
						   validationError = validationError && res;
							break;

	                     case "actualDateLessThan":
	                        var res=actualDateLessThanValidator(parameters[i], validations[j].dateLessThanVar);
							validationError = validationError && res;
							break;	
					}

					if (!res) {
						if (_.isUndefined($rootScope.pageErrorCode[parameters[i]])) {
							$rootScope.pageErrorCode[parameters[i]] = validations[j].validationType;
						}
					}
				}
			}

			// for (var param in parameters) {
			// 	WL.Logger.fatal("Parameters in ValidationService: " + JSON.stringify(parameters));
			// 	var validations = ValidationProcessor.getValidationsForField(parameters[param]);
			// 	for(var validate in validations){
			// 		WL.Logger.fatal("Validations in ValidationService: " + JSON.stringify(validations));
			// 		for(var valType in validations[validate]){
			// 			var validationObj = validations[validate];
			// 			var validationType = validationObj["validationType"]
			// 			WL.Logger.fatal("Validations in ValidationService: " + JSON.stringify(validations));
			// 			switch(validationType){
			// 				case "required":
			// 					validationError = validationError && requiredValidator(parameters[param]);
			// 					break;
			// 				case "maxLength":
			// 					validationError = validationError && maxLengthValidator(parameters[param], validationObj["maxLengthValue"]);
			// 					break;
			// 				case "minLength":
			// 					validationError = validationError && minLengthValidator(parameters[param], validationObj["minLengthValue"]);
			// 					break;
			// 			}		
			// 		}
			// 	}
			// }
			WL.Logger.fatal("Validation Error in service: " + validationError);
			//In ActionProcessor, the value is form Valid.Hence the negation.
			return validationError;
		},	
	}
}]);