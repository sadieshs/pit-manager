var AppController = angular.module("AppController");
AppController.factory('FormProcessor', ['$http', '$q', '$location','$rootScope',
                               function($http, $q, $location, $rootScope) {
	// variables that will be used across this service
	// Each feature is a group of subFeatures. What we really want to
	// remember is the subFeatures and their workflows
	var allSubFeaturesList = [];
	
	// function definitions
	return {
		// The init method
		init: function() {
			
			// Remember, we need to return a promise
			var deferred = $q.defer();
			
			// Let us read the feature config file now. This is the FeatureProcessor
		    $http.get('navigation/formConfig.json').
		    success(function(allFeaturesData) {
		    	
		    	// Read each feature and its subFeatures
		    	var fileOpens = [];
		    	for (var feature in allFeaturesData) {
		    		var featureData = allFeaturesData[feature];
		    		var subFeatures = featureData.subFeatures;
		        			
		    		// Now, iterate through each subfeature to read the workflow
		    		var subFeatureIndex = 0;
		    		for (subFeatureIndex = 0; subFeatureIndex < subFeatures.length; subFeatureIndex++) {
		    			
		    			var subFeature = subFeatures[subFeatureIndex];
		    			var workflowTemplateFile = subFeature.workflowTemplateFile;
		    			var thisFeatureJson = {};
		    			//thisFeatureJson["subFeatureName"] = subFeature.name;
		    			
		    			// Let us read the data file for each feature and save the
		    			// promises returned by them
		    			
		    			fileOpens.push($http.get(workflowTemplateFile));
		    		}
		    	}
		    		
		    	// We have done issuing commands to read all the files
		    	// Let us now wait for all operations to complete
		    	$q.all(fileOpens).then(
    				function(payloads) {
    					// all successfully read.
    					// Let us read all the features and save them in memory
                        $rootScope.masterForm=[];
                           for (var index = 0; index < payloads.length; index++) {
                               $rootScope.masterForm[payloads[index].data.formName]=payloads[index].data[payloads[index].data.formName];
                           }
                           deferred.resolve();
    				},
		    		function(errorPayloads) {
    					// all error
    					deferred.reject(errorPayloads);
		    		},
		    		function(updates) {
		    			deferred.update(updates);
		    		}
		    	); // done reading all feature files
		    }).error(function(msg, code) {
		    	// Lets log an error here
		    }); // Done reading the features config file
		    // Let us return the promise now
		    return deferred.promise;
		},
		// The method which fetches the start page for a given feature
		getStartPageForSubFeature: function(feature) {
			// First, let us find the feature.
			var index;
			for (index = 0; index < allSubFeaturesList.length; index++) {
				var featureJson = allSubFeaturesList[index];
				
				if (featureJson.subFeatureName == feature) {
					// This is the one we were looking for
					return featureJson.startPage;
				}
			}
			// If we are here, it means this subfeature does not exist
			return '';
		},
		// The method that fetches the workflow for a given page
		getWorkflowForSubFeature: function(subFeature) {
			// First, let us find the feature.
			var index;
			for (index = 0; index < allSubFeaturesList.length; index++) {
				var featureJson = allSubFeaturesList[index];
				
				if (featureJson.subFeatureName == subFeature) {
					// This is the one we were looking for
					return featureJson.workflow;
				}
			}
			// If we are here, it means this subfeature does not exist
			return '';
		}
	};
}]);