var AppController = angular.module("AppController");
AppController.factory('LocaleProcessor', ['$http', '$q','$rootScope',
                               function($http, $q, $rootScope) {
	// variables that will be used across this service
	var thisUserLocale = ''; // Contains the current locale setting for the user 
	var literals = {};
	
	// function definitions
	return {
		
		// The init method which initializes processing of locales
		// param: Expects the mobile app configuration as an input as
		// localte settings are part of mobile app settings
		init: function(mobileAppConfig) {
			
			// Note that we will have to return a promise because the caller
			// is waiting on us to complete some functionality
			var deferred = $q.defer();
			
			this.setLocaleForUser(mobileAppConfig.defaultLocale).then(function(localeData) {
				//WL.Logger.debug("Read the locale successfully successfully");
				deferred.resolve(localeData);
			}, function(errorLocaleData) {
				//WL.Logger.debug("Error processing locale configs");
				deferred.resolve(errorLocaleData);
			});
		    
		    // Let us return the promise now
		    return deferred.promise;
		},
		// The method which sets the locale for this user
		setLocaleForUser: function(locale) {
			// Note that we will have to return a promise because the caller
			// is waiting on us to complete some functionality
			var deferred = $q.defer();
			var localeFilePath = '';
			
			// Let us now process the locale
			// Remember the locale we are setting to
			thisUserLocale = locale;
			localeFilePath = 'locale/Literals_' + thisUserLocale + '.json';
			// Let us read the locale file now
		    $http.get(localeFilePath).success(function(fileData) {
		    	// The only thing to do is to load the contents of the literal file
		    	literals = fileData;
		    	//WL.Logger.debug("Successfully set the locale to " + locale);
		    	deferred.resolve(fileData);
		    }).error(function(msg, code) {
		    	deferred.reject(msg);
		    });
		    
		    // Let us return the promise now
		    return deferred.promise;
		},
		// The method which fetches the current literals data
		getLocalizedLiterals: function() {
			return literals;
		}
	};
}]);