var AppController = angular.module("AppController");
AppController.factory('Logger', ['LoggerImpl',
                               function(LoggerImpl) {
	// Handle to the actual logger implementation instance.
	
	// It is highly possible that the constructor init was not
	// called due to bad coding practice. In order to handle such
	// a case, we either check for nulls in each method, or we initialize
	// the default instance to a logger that does not print anything.
	// In this case, we choose the latter
	var loggerInstance = LoggerImpl;
	
	// function definitions
	return {
		
		// The init method which sets up the logger for this factory
		init: function(loggerImplInstance) {
			loggerInstance = loggerImplInstance;
		},
		// The method that logs the message at info level
		info: function(message) {
			loggerInstance.info(message);
		},
		// The method that logs the message at debug level
		debug: function(message) {
			loggerInstance.debug(message);
		},
		// The method that logs the message at fatal level
		fatal: function(message) {
			loggerInstance.fatal(message);
		}
	};
}]);