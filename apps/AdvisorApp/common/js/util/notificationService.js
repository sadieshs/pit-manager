function notificationService(msg){
	var notiArr=msg.split(':');
	if(notiArr.length>0){
		var notiType=notiArr[0];
		var notification=notiArr[1];
		
		toastr.options = {
					"closeButton": false,
					"debug": false,
					"newestOnTop": false,
					"progressBar": false,
					"positionClass": "toast-top-right",
					"preventDuplicates": false,
					"onclick": null,
					"showDuration": "300",
					"hideDuration": "1000",
					"timeOut": "5000",
					"extendedTimeOut": "1000",
					"showEasing": "swing",
					"hideEasing": "linear",
					"showMethod": "fadeIn",
					"hideMethod": "fadeOut"
					}
		if(notiType=='success'){
			toastr.options.positionClass="toast-top-right";
		}
		else if(notiType=='info') {
			toastr.options.positionClass="toast-top-right";
		}
		else if(notiType=='warning') {
			toastr.options.positionClass="toast-top-right";
		}
		else if(notiType=='error') {
			toastr.options.positionClass="toast-top-right";
		}
		else if(notiType=='SUCCESS') {
			toastr.options.positionClass="toast-top-full-width";
		}
		else if(notiType=='INFO') {
			toastr.options.positionClass="toast-top-full-width";
		}
		else if(notiType=='WARNING') {
			toastr.options.positionClass="toast-top-full-width";
		}
		else if(notiType=='ERROR') {
			toastr.options.positionClass="toast-top-full-width";
		}
		toastr[notiType.toLowerCase()](notification);
	}
}