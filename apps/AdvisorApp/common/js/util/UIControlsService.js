
/* JavaScript content from js/utils/UIControlsService.js in folder common */

/* JavaScript content from js/utils/UIControlsService.js in folder common */

/* JavaScript content from js/utils/UIControlsService.js in folder common */
var AppController = angular.module("AppController");

AppController.factory('UIControlsService', ['$rootScope', '$compile','$location',
                               function($rootScope, $compile,$location) {
    return {
        init: function() {

            // create UI variables          
            if(_.isUndefined($rootScope.showMenu)){
                $rootScope.showMenu = false;
            }
        }, 
        showConfirmationScreen: function(title, message, okText, cancelText) {
            // $("body").append('<confirmation-screen id="confirmation-screen" message="Hello world"></confirmation-screen>');
            $rootScope.confirmationObj = {
                                            title: title,
                message: message,
                okText: _.isUndefined(cancelText) ? "OK" : okText,
                cancelText: _.isUndefined(cancelText) ? null : cancelText
            }; 
            $location.path("navigation/common/resources/FinacleConfirmationPage");
            //$("body").append('<div confirmation-screen id="confirmation-screen"></div>');
            //$compile(document.body)($rootScope);
        },
        showValidationAlert: function(message, okText) {
            $rootScope.alertOverlayObj = {
                message: message,
                okText: _.isUndefined(okText) ? "OK" : okText,
                isCancel: true
            }; 
            $rootScope.showValidationAlertFlag = true;
            // $("body").append('<div alert-overlay-screen id="alert-overlay-screen"></div>');
            // $compile(document.body)($rootScope);
        },
        hideValidationAlert: function() {
          $rootScope.showValidationAlertFlag = false;
        },
        showAlertOverlayScreen: function(message, okText) {
            // $("body").append('<confirmation-screen id="confirmation-screen" message="Hello world"></confirmation-screen>');
            $rootScope.alertOverlayObj = {
                message: message,
                okText: _.isUndefined(okText) ? "OK" : okText,
                isCancel:true
            }; 
            $rootScope.showAlertFlag = true;
            // $("body").append('<div alert-overlay-screen id="alert-overlay-screen"></div>');
            // $compile(document.body)($rootScope);
        },
        showSuccessOverlayScreen: function(message, okText) {
            // $("body").append('<confirmation-screen id="confirmation-screen" message="Hello world"></confirmation-screen>');
            $rootScope.alertOverlayObj = {
                message: message,
                okText: _.isUndefined(okText) ? "OK" : okText,
                isCancel:false
            }; 
            $rootScope.showAlertFlag = true;
            // $("body").append('<div alert-overlay-screen id="alert-overlay-screen"></div>');
            // $compile(document.body)($rootScope);
            //return;
        },
        hideAlertOverlayScreen: function() {
            $rootScope.showAlertFlag = false; 
        },
        isToggleMenuVisible: function(){
            return $rootScope.showMenu;
        },
        toggleHideMenu: function(){
            $rootScope.showMenu = true;
            this.toggleShowMenu();
        },
        toggleShowMenu:  function() {
            if(_.isUndefined($rootScope.showMenu)){
                $rootScope.showMenu = false;
            } 
            $rootScope.showMenu = !$rootScope.showMenu;  
            if ($rootScope.showMenu) {
                $("#fms-content").css("margin-left", "0");
            } else {
                $("#fms-content").css("margin-left", "-16em");
            }
        },
        gotoPage :function(path) {
            $rootScope.showMenu = true;
            toggleShowMenu();    
            $location.path(path);  
        },
        createDropDownOptionArray: function(dataArr, keyStrArr, valueStr, beginStrArr, endStrArr) {
          var result = [];
          if(dataArr != null){
    	  	var preStr = "";
    	  	var preIndex=-1;
	        if (beginStrArr != null) {
		        for (var i = 0; i < beginStrArr.length; i++) {
		        	//if (_.isUndefined(dataArr[0][beginStrArr[i]])) { //check if any entry in the array is part of the data array
		        		 preStr += beginStrArr[i];
//		        	}
//		        	else
//		        		preIndex=i;
		        }
	        }	
 
             var postStr = "";
             var postIndex=-1;
             if (endStrArr != null) {
               for (var i = 0; i < endStrArr.length; i++) {
            	   //if (_.isUndefined(dataArr[0][endStrArr[i]])) { //check if any entry in the array is part of the data array
            		   postStr += endStrArr[i];
//            	   }
//            	   else{
//            		   postIndex=i;
//            	   }
               	}
             }
        	  for (var i = 0; i < dataArr.length; i++) {
          
	            var keyStr = "";
	            for (var j = 0; j < keyStrArr.length; j++) {
	              if (_.isUndefined(dataArr[i][keyStrArr[j]])) {
	                keyStr += (keyStrArr[j].length == 0 ? " " : keyStrArr[j]);
	              } else {
	                keyStr += dataArr[i][keyStrArr[j]];
	              }
	            }
	            result[i] = { "label": preStr + keyStr + postStr, "value": dataArr[i][valueStr]};
	          }
          }

          return result;
        },
        transactionSearchSetup: function(byDateOption, date1, date2) {

            if (_.isUndefined(byDateOption) || byDateOption == 0) {
            	$rootScope.fields.trxnSearchByDate1 = "";
            	$rootScope.fields.trxnSearchByDate2 = "";
                return;
            }

            if (byDateOption < 4) {
                var totalDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
                var currdate = new Date();

                var date = currdate.getDate();
                var month = currdate.getMonth() +  1;  
                var year = currdate.getFullYear();

                $rootScope.fields.txnSrcToDate_day = "" + date + "";
                $rootScope.fields.txnSrcToDate_month = "" + month + "";
                $rootScope.fields.txnSrcToDate_year = "" + year + "";

                switch (byDateOption) {
                    case 1: 
                          if (date < 7) {
                          
                            if (month == 1) {
                              month = 12;
                              year--;


                            } else {
                              month--;
                            }
                            monthDays = totalDays[month-1];
                            monthDays += (month == 2 && ((year % 4) == 0)) ? 1 : 0;
                            date = monthDays - (7 - date) ;
                          } else {
                            date -= 7;
                          }
                          break;

                    case 2: 

                          if (month == 1) {
                              month = 12;
                              year--;
                            } else {
                              month--;
                            }
                          break;          

                    case 3: 

                          if (month < 3) {
                              month = 12 - (3 - month);
                              year--;
                            } else {
                              month -= 3;
                            }
                          break;                    
                }

                $rootScope.fields.txnSrcFromDate_day = "" + date + "";
                $rootScope.fields.txnSrcFromDate_month = "" + month + "";
                $rootScope.fields.txnSrcFromDate_year = "" + year + "";
                
                $rootScope.fields.trxnSearchByDate1 = $rootScope.fields.txnSrcFromDate_year + "-" + $rootScope.fields.txnSrcFromDate_month + "-" + $rootScope.fields.txnSrcFromDate_day;                			
                $rootScope.fields.trxnSearchByDate2 = $rootScope.fields.txnSrcToDate_year + "-" + $rootScope.fields.txnSrcToDate_month + "-" + $rootScope.fields.txnSrcToDate_day;                                        

            } else {
            	
                switch (byDateOption) {
                	case 4: 
                        if (date1 == null || date1 == "") { 
                        	$rootScope.fields.trxnSearchByDate1 = "";
                        	$rootScope.fields.trxnSearchByDate2 = "";
                        } else {
                        	// var arr = date1.split("-");

                         //    $rootScope.fields.txnSrcFromDate_day = arr[2];
                         //    $rootScope.fields.txnSrcFromDate_month = arr[1];
                         //    $rootScope.fields.txnSrcFromDate_year = arr[0]; 
                         //    $rootScope.fields.trxnSearchByDate1 = $rootScope.fields.txnSrcFromDate_year + "-" + $rootScope.fields.txnSrcFromDate_month + "-" + $rootScope.fields.txnSrcFromDate_day;

                         //    $rootScope.fields.txnSrcToDate_day = arr[2];
                         //    $rootScope.fields.txnSrcToDate_month = arr[1];
                         //    $rootScope.fields.txnSrcToDate_year = arr[0];
                         //    $rootScope.fields.trxnSearchByDate2 = $rootScope.fields.txnSrcToDate_year + "-" + $rootScope.fields.txnSrcToDate_month + "-" + $rootScope.fields.txnSrcToDate_day;                        

                            $rootScope.fields.txnSrcFromDate_day = "" + date1.getDate() + "";
                            $rootScope.fields.txnSrcFromDate_month = "" + (date1.getMonth() + 1) + "";
                            $rootScope.fields.txnSrcFromDate_year = "" + date1.getFullYear() + ""; 
                            $rootScope.fields.trxnSearchByDate1 = $rootScope.fields.txnSrcFromDate_year + "-" + ($rootScope.fields.txnSrcFromDate_month < 10 ? "0" : "") + $rootScope.fields.txnSrcFromDate_month + "-" + $rootScope.fields.txnSrcFromDate_day;

                            $rootScope.fields.txnSrcToDate_day = "" + date1.getDate() + "";
                            $rootScope.fields.txnSrcToDate_month = "" + (date1.getMonth() + 1) + "";
                            $rootScope.fields.txnSrcToDate_year = "" + date1.getFullYear() + ""; 
                            $rootScope.fields.trxnSearchByDate2 = $rootScope.fields.txnSrcToDate_year + "-" + ($rootScope.fields.txnSrcToDate_month < 10 ? "0" : "") + $rootScope.fields.txnSrcToDate_month + "-" + $rootScope.fields.txnSrcToDate_day;                        

                        }
                		break;
                		
                	case 5: 
                		
                		if (date1 == null || date1 == "") { 
                        	$rootScope.fields.trxnSearchByDate1 = "";
                		} else {
                            // var arr = date1.split("-");

                            // $rootScope.fields.txnSrcFromDate_day = arr[2];
                            // $rootScope.fields.txnSrcFromDate_month = arr[1];
                            // $rootScope.fields.txnSrcFromDate_year = arr[0]; 
                            // $rootScope.fields.trxnSearchByDate1 = $rootScope.fields.txnSrcFromDate_year + "-" + $rootScope.fields.txnSrcFromDate_month + "-" + $rootScope.fields.txnSrcFromDate_day;                			

                            $rootScope.fields.txnSrcFromDate_day = "" + date1.getDate() + "";
                            $rootScope.fields.txnSrcFromDate_month = "" + (date1.getMonth() + 1) + "";
                            $rootScope.fields.txnSrcFromDate_year = "" + date1.getFullYear() + ""; 
                            $rootScope.fields.trxnSearchByDate1 = $rootScope.fields.txnSrcFromDate_year + "-" + ($rootScope.fields.txnSrcFromDate_month < 10 ? "0" : "") + $rootScope.fields.txnSrcFromDate_month + "-" + $rootScope.fields.txnSrcFromDate_day;                      

                		}
                		
                		if(date2 == null || date2 == ""){
                        	$rootScope.fields.trxnSearchByDate2 = "";
                		} else {
                            // var arr = date2.split("-");

                            $rootScope.fields.txnSrcToDate_day = "" + date2.getDate() + "";
                            $rootScope.fields.txnSrcToDate_month = "" + (date2.getMonth() + 1) + "";
                            $rootScope.fields.txnSrcToDate_year = "" + date2.getFullYear() + ""; 
                            $rootScope.fields.trxnSearchByDate2 = $rootScope.fields.txnSrcToDate_year + "-" + ($rootScope.fields.txnSrcToDate_month < 10 ? "0" : "") + $rootScope.fields.txnSrcToDate_month + "-" + $rootScope.fields.txnSrcToDate_day;                            
                			
                		}
                		
                		break;
                }
            } 
        }
                                            
    }                        
}]);

AppController.directive('confirmationScreen', function() {
  return {
    restrict: 'AE',
    templateUrl: 'navigation/common/resources/FinacleConfirmationPage.html'
  };
});

//Menu added

AppController.directive('calendar',function(){
  return {
    templateUrl: 'partial/calendar.html',
    controller: 'AppController'
  };
})

AppController.directive('day',function($timeout, $window) {
  return { require: '^calendar', link: link };
  function link(scope, element, attr, ctrl) {}

})

AppController.directive('sideMenuLeft',function(){
  return{
        restrict:'E',
        templateUrl:'partial/sideMenu.html'
  }
})

AppController.directive('alertOverlayScreen', function() {
      return {
        restrict: 'AE',
        templateUrl: 'navigation/common/resources/FinacleLoaderErrorPage.html'
      };
});

AppController.directive('amountInputValidate', function($rootScope) {
  return function(scope, element, attrs) {
    element.bind(
      "keypress",
      function(event) {                                     
        if (event.keyCode != 46 && (event.keyCode < 0x30 || event.keyCode > 0x39)) {
          event.preventDefault();
        } else if (event.keyCode == 46 && (element[0].value.indexOf('.') != -1)) {
            event.preventDefault();
        } else {
          var money = element[0].value.split(".");
          if (!_.isUndefined(money[1]) && money[1].length >= 2) {
            event.preventDefault();
          }
        }                               
      }
    )
  }
});

AppController.directive('numberInputValidate', function($rootScope) {
  return function(scope, element, attrs) {
    element.bind(
      "keypress",
      function(event) {
        if (event.keyCode < 0x30 || event.keyCode > 0x39) {
          event.preventDefault();
        }                              
      }
    )
  }
});

AppController.directive('passwordInputValidate', function($rootScope) {
  return function(scope, element, attrs) {
    element.bind(
      "keypress",
      function(event) {
        if (event.keyCode == 0x20) {
          event.preventDefault();
        }                              
      }
    )
  }
})

AppController.directive('clearWarning', function($rootScope) {
  return function(scope, element, attrs) {
    element.bind(
      "keypress",
      function(event) {
        if (!_.isUndefined($rootScope.pageErrorArr)) {
          $rootScope.pageErrorArr[attrs.clearWarning] = null;
        }
      }
    )
  }
})

AppController.directive('repeatDone',function($rootScope, $compile){
	return function(scope, element, attrs) {
        if (scope.$last) { // all are rendered
            scope.$eval(attrs.repeatDone);
        }
    }
});

AppController.directive('finInput', function($rootScope, $compile) {

  var getTemplate = function(scope, displayRow, directiveType, ngModelVar, placeholder, valFlag, optionArr, readOnlyFlag, maxLength, ngChange) {

    var errorTemplate = 
      "<div class=\"fin-input-container\"> \
        <p class=\"error-text\"> ERROR! " + directiveType + "</p> \
      </div>";
    
    if (_.isUndefined(displayRow)){
      displayRow = "full";
    } 

    if (_.isUndefined(directiveType)){
      directiveType = "text";
    } 

    if (_.isUndefined(ngModelVar)){
      ngModelVar = "null";
    } 

    if (_.isUndefined(placeholder)){
      placeholder = "";
    }

    if (_.isUndefined(valFlag)){
      valFlag = "null";
    }

    var firstline = "";
    switch(displayRow) {
      case "full":
        firstline = "<div class=\"fin-input-container\">";
        break;
      case "half1":
        firstline = "<div class=\"fin-input-container fin-input-container-half1\">";
        break; 
      case "half2":
        firstline = "<div class=\"fin-input-container fin-input-container-half2\">";
        break;                    
      case "qtr1":
        firstline = "<div class=\"fin-input-container fin-input-container-qtr1\">";
        break;                    
      default: return errorTemplate; 
    }     
      
      var resTemplate = "";
      switch(directiveType) {
        case "text":
          resTemplate = 
              firstline +
                "<div ng-class='{\"left-line\": !" + valFlag + ", \"left-line-error\":" + valFlag + "}'> \
                </div> \
                <div class=\"input-body\"> \
                  <div class=\"input-body2\"> \
                    <input class=\"fin-input\" type=\"text\" ng-model=\"" + ngModelVar + "\" placeholder=\"" + placeholder + "\"" + (_.isUndefined(readOnlyFlag) ? "" : " disabled") + (_.isUndefined(maxLength) ? "" : " maxlength=" + maxLength) + (_.isUndefined(ngChange) ? "" : " ng-change=" + ngChange) + "> \
                  </div> \
                  <div ng-class='{\"bottom-line\": !" + valFlag + ", \"bottom-line-error\":" + valFlag + "}'> \
                  </div> \
                </div> \
                <p class=\"warning-text\" ng-show='" + valFlag + "'> ! </p> \
                <div ng-class='{\"right-line\": !" + valFlag + ", \"right-line-error\":" + valFlag + "}'> \
                </div> \
              </div>";          
          break;
        case "password_text":
          resTemplate = 
              firstline +
                "<div ng-class='{\"left-line\": !" + valFlag + ", \"left-line-error\":" + valFlag + "}'> \
                </div> \
                <div class=\"input-body\"> \
                  <div class=\"input-body2\"> \
                    <input password-input-validate class=\"fin-input\" type=\"password\" ng-model=\"" + ngModelVar + "\" placeholder=\"" + placeholder + "\"" + (_.isUndefined(readOnlyFlag) ? "" : " disabled") + (_.isUndefined(maxLength) ? "" : " maxlength=" + maxLength) + (_.isUndefined(ngChange) ? "" : " ng-change=" + ngChange) + "> \
                  </div> \
                  <div ng-class='{\"bottom-line\": !" + valFlag + ", \"bottom-line-error\":" + valFlag + "}'> \
                  </div> \
                </div> \
                <p class=\"warning-text\" ng-show='" + valFlag + "'> ! </p> \
                <div ng-class='{\"right-line\": !" + valFlag + ", \"right-line-error\":" + valFlag + "}'> \
                </div> \
              </div>";          
          break;
        case "password_num":
          resTemplate = 
              firstline +
                "<div ng-class='{\"left-line\": !" + valFlag + ", \"left-line-error\":" + valFlag + "}'> \
                </div> \
                <div class=\"input-body\"> \
                  <div class=\"input-body2\"> \
                    <input class=\"fin-input\" number-input-validate type=\"password\" ng-model=\"" + ngModelVar + "\" placeholder=\"" + placeholder + "\" " + (_.isUndefined(readOnlyFlag) ? "" : " disabled") + (_.isUndefined(maxLength) ? "" : " maxlength=" + maxLength) + (_.isUndefined(ngChange) ? "" : " ng-change=" + ngChange) + "> \
                  </div> \
                  <div ng-class='{\"bottom-line\": !" + valFlag + ", \"bottom-line-error\":" + valFlag + "}'> \
                  </div> \
                </div> \
                <p class=\"warning-text\" ng-show='" + valFlag + "'> ! </p> \
                <div ng-class='{\"right-line\": !" + valFlag + ", \"right-line-error\":" + valFlag + "}'> \
                </div> \
              </div>";          
          break;              
        case "money":
          resTemplate = 
              firstline +
                "<div ng-class='{\"left-line\": !" + valFlag + ", \"left-line-error\":" + valFlag + "}'> \
                </div> \
                <div class=\"input-body\"> \
                  <div class=\"input-body2\"> \
                    <input amount-input-validate class=\"fin-input\" type=\"number\" ng-model=\"" + ngModelVar + "\" placeholder=\"" + placeholder + "\"" + (_.isUndefined(readOnlyFlag) ? "" : " disabled") + (_.isUndefined(ngChange) ? "" : " ng-change=" + ngChange) + "> \
                  </div> \
                  <div ng-class='{\"bottom-line\": !" + valFlag + ", \"bottom-line-error\":" + valFlag + "}'> \
                  </div> \
                </div> \
                <p class=\"warning-text\" ng-show='" + valFlag + "'> ! </p> \
                <div ng-class='{\"right-line\": !" + valFlag + ", \"right-line-error\":" + valFlag + "}'> \
                </div> \
              </div>";          
          break;
        case "number":
          resTemplate = 
              firstline +
                "<div ng-class='{\"left-line\": !" + valFlag + ", \"left-line-error\":" + valFlag + "}'> \
                </div> \
                <div class=\"input-body\"> \
                  <div class=\"input-body2\"> \
                    <input number-input-validate class=\"fin-input\" type=\"number\" min=\"0\" pattern=\"[0-9]*\" ng-model=\""  + ngModelVar + "\" placeholder=\"" + placeholder + "\"" + (_.isUndefined(maxLength) ? "" : " ng-maxlength=" + maxLength) + (_.isUndefined(readOnlyFlag) ? "" : " disabled") + (_.isUndefined(ngChange) ? "" : " ng-change=" + ngChange) + "> \
                  </div> \
                  <div ng-class='{\"bottom-line\": !" + valFlag + ", \"bottom-line-error\":" + valFlag + "}'> \
                  </div> \
                </div> \
                <p class=\"warning-text\" ng-show='" + valFlag + "'> ! </p> \
                <div ng-class='{\"right-line\": !" + valFlag + ", \"right-line-error\":" + valFlag + "}'> \
                </div> \
              </div>";          
          break;          
        case "date":
          resTemplate = 
              firstline +
                "<div ng-class='{\"left-line\": !" + valFlag + ", \"left-line-error\":" + valFlag + "}'> \
                </div> \
                <div class=\"input-body\"> \
                  <span class=\"icon icon-calendar\"  ng-class='{\"icon-calendar-input\": !" + valFlag + ", \"icon-calendar-input-error\":" + valFlag + "}' ng-style='" + ngModelVar + " == null || " + ngModelVar + " == \"\" ? {\"top\": \"-0.1em\"} : {\"top\": \"-0.95em\"}'></span>  \
                  <div class=\"place-holder\" ng-show='" + ngModelVar + " == null || " + ngModelVar + " == \"\"'>  \
                    <span>" + placeholder + "</span> \
                  </div> \
                  <div class=\"input-body2 date-adjust\"> \
                    <input class=\"fin-input "+ (displayRow == "full" ? "" : " fin-input-date") + "\" type=\"date\" ng-model=\"" + ngModelVar + "\" ng-click='" + ngModelVar + " = \"\" '" + (_.isUndefined(readOnlyFlag) ? "" : " disabled") + (_.isUndefined(ngChange) ? "" : " ng-change=" + ngChange) +  "> \
                  </div> \
                  <div ng-class='{\"bottom-line\": !" + valFlag + ", \"bottom-line-error\":" + valFlag + "}'> \
                  </div> \
                </div> \
                <p class=\"warning-date\" ng-show='" + valFlag + "'> ! </p> \
                <div ng-class='{\"right-line\": !" + valFlag + ", \"right-line-error\":" + valFlag + "}'> \
                </div> \
              </div>"; 

          break; 
        case "label": 
          resTemplate = 
              firstline +
                "<div class=\"input-body\"> \
                  <div class=\"place-holder\" ng-show=\"" + ngModelVar + " == null\">  \
                    <span>" + placeholder + "</span> \
                  </div> \
                  <div class=\"label-holder\" ng-hide=\"" + ngModelVar + " == null\">  \
                    <span> {{" + ngModelVar + "}}</span> \
                  </div> \
                  <div ng-class='{\"bottom-line\": !" + valFlag + ", \"bottom-line-error\":" + valFlag + "}'> \
                  </div> \
                </div> \
                <p class=\"warning-text warning-select\" ng-show='" + valFlag + "'> ! </p> \
                <span ng-class='{\"dropdown\": !" + valFlag + ", \"dropdown-error\": " + valFlag + "}'> </span> \
              </div>";
          break;          
        case "dropdown":
          if (_.isUndefined(optionArr) || optionArr.length == 0) {
            return errorTemplate;             
          }

          resTemplate = 
              firstline +
                "<div class=\"input-body\"> \
                  <div class=\"input-body2 input-body2-dropdown\"> \
                    <select class=\"fin-input fin-select\" ng-model='" + ngModelVar + "' \
                      ng-style='" + ngModelVar + "== null || " + ngModelVar + " == \"\" ? {\"color\": \"#aaa\"} : {\"color\": \"initial\"}' " + (_.isUndefined(readOnlyFlag) ? "" : " disabled") + (_.isUndefined(ngChange) ? "" : " ng-change=" + ngChange) + "> \
                      <option class=\"place-holder-select\" value=\"\" ng-selected=\"" + ngModelVar + "== null\"" + " ng-show=\"" + ngModelVar + "== null\">" + placeholder + "</option> \
                      <option ng-repeat=\"option in " + optionArr + "\" ng-selected='option.value==\"" + scope.$eval(ngModelVar) + "\"' value=\"{{option.value}}\">{{option.label}}</option> \
                    </select> \
                  </div> \
                  <div ng-class='{\"bottom-line\": !" + valFlag + ", \"bottom-line-error\":" + valFlag + "}'> \
                  </div> \
                </div> \
                <p class=\"warning-text warning-select\" ng-show='" + valFlag + "'> ! </p> \
                <span ng-class='{\"dropdown\": !" + valFlag + ", \"dropdown-error\": " + valFlag + "}'> </span> \
              </div>";           
          break;                    
        default: return errorTemplate; 
      }        

      return resTemplate;       
    }
 

  return {
    link: 
      function(scope, element, attrs) {
        scope.placeholder = attrs.placeholder;
        ngModelVar = attrs.model;
        scope.model = scope.$eval(attrs.model);

        var template1 = getTemplate(scope, attrs.displayRow, attrs.type, attrs.model, attrs.placeholder, attrs.validationFlag, attrs.optionArray, attrs.readonly, attrs.maxLength, attrs.onChange);

        element.html(template1).show();
        $compile(element.contents())(scope);

      }
    }
});