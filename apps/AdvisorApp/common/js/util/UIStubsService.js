/* JavaScript content from js/utils/UIStubsService.js in folder common */

AppController.factory('UIStubsService', ['$rootScope', function($rootScope) {

var messageList = [

/*// LOGIN{
    REQUEST:{"adapter":"AuthenticationService","procedure":"processRequest","parameters":[{"CorpId":"","PassWord":""}]},
    RESPONSE:{"status":200,"invocationContext":null,"invocationResult":{"customerName":"JAMES","userType":"1","sessKey":"559BB355791E8469F621DF716FD0A9A0","responseHeaders":{"Date":"Thu, 26 Feb 2015 09:00:35 GMT","Expires":"0","Server":"Apache-Coyote/1.1","Cache-Control":"no-cache","Pragma":"no-cache"},"mbParam":"hDurDhVmjJo6ZK+5Euihci8jEwot6tQxWlJ2fb8TrDfy0nrS6dkLLT1ijw==","statusCode":200,"lastLogin":"26-02-2015 02:30:34 PM","operativeAccountsList":[{"accountIndex":"0","mainAccountType":"OPR","balance":"50010.0 INR","accountId":"xxxxxxxx01","accountType":"SBA","subAccountTypeDesc":"Savings","accountNickName":"My USD Account","currency":"USD"},{"accountIndex":"1","mainAccountType":"OPR","balance":"5000.0 INR","accountId":"xxxxxxxx02","accountType":"SBA","subAccountTypeDesc":"Savings","accountNickName":"My EUR Account","currency":"EUR"},{"accountIndex":"2","mainAccountType":"OPR","balance":"20000.0 INR","accountId":"xxxxxxxx01","accountType":"CAA","subAccountTypeDesc":"Current","accountNickName":"","currency":"INR"}],"isSuccessful":true,"statusReason":"OK","depositAccountsList":[{"accountIndex":"0","mainAccountType":"DEP","balance":"0.0 ","accountId":"xxxxxxxx01","accountType":"TDA","subAccountTypeDesc":"Term Deposit","accountNickName":"Plus Deposit","currency":"INR"},{"accountIndex":"1","mainAccountType":"DEP","balance":"0.0 ","accountId":"xxxxxxxx04","accountType":"TUA","subAccountTypeDesc":"Top Up Deposit","accountNickName":"Top Up Deposit","currency":"INR"}],"ccList": [ { "creditCardNo":"xxxxxxxxxxxx1100", "amountDue":"530.0 INR", "dueDate":"", "index":"0","accountType":"platenium card"},{ "creditCardNo":"xxxxxxxxxxxx1101", "amountDue":"531.0 INR", "dueDate":"", "index":"1","accountType":"corporate card" } ],"WL-Authentication-Success":{"SingleStepAuthRealm":{"userId":"james","isUserAuthenticated":1,"attributes":{"foo":"bar"},"displayName":"james"}},"loanAccountsList":[{"accountIndex":"0","mainAccountType":"LON","balance":"50000.0 INR","accountId":"xxxxxxxx01","accountType":"LAA","subAccountTypeDesc":"Loan","accountNickName":"Car Loan","currency":"INR"}],"totalTime":362,"responseTime":362}}
    //RESPONSE:{"status":200,"invocationContext":null,"invocationResult":[{ "errorMessage":"Login and Transaction Passwords Expired","forceLoginPassword":"TRUE","forceTxnPassword":"TRUE", "sessKey":"dO2xrl5sFUFHkEEFVR6ZqFc0", "mbParam":"hDurDuQ1dr8EeYpoQLMuI%2BcD%2BICODShj1EPZ0izQ3gcuc%2FRbZb53wwt6rG8t"}]}
},
// LOGOUT
{
    REQUEST:{"adapter":"LogoutService","procedure":"processRequest","parameters":[{"jsessionid":"559BB355791E8469F621DF716FD0A9A0","mbparam":"hDurDhVmjJo6ZK+5Euihci8jEwot6tQxWlJ2fb8TrDfy0nrS6dkLLT1ijw=="}]},
    RESPONSE:{"status":200,"invocationContext":null,"invocationResult":{"response":{"responsesList":[{"message":"LOGOUT SUCCESS","statusCode":200,"isSuccessful":true,"statusReason":"OK","responseHeaders":{"Date":"Thu, 26 Feb 2015 09:01:38 GMT","Content-Type":"text/html;charset=ISO-8859-1", "Server":"Apache-Coyote/1.1"}, "responseTime":143, "totalTime":143}], "isSuccessful": "true"},"isSuccessful":true}}
},*/

// {
//     REQUEST:{"adapter":"DashboardService"},
//     RESPONSE:{"status":200,"invocationContext":null,"invocationResult":{"response":{"responsesList":[{"pageData":{"alert":[{"customerName":"Henry","message":"Call Henry for further details","time":"3:30","type":"newCustomer"},{"customerName":"Aman","message":"Followup with Aman for premium","type":"followUp"},{"customerName":"Gita","message":"Notify Gita about platinum benifits","type":"newCustomer"},{"customerName":"Rama","message":"Followup with Aman for credit balance","type":"followUp"}],"stock":[{"stockName":"BSE Sensex","price":"26,525.46","change":"-200.88","pChange":"-0.75"},{"stockName":"Gold","price":"1,306.90","change":"+18.60","pChange":"+1.44"},{"stockName":"Oil","price":"46.29","change":"-1.72","pChange":"-3.58"},{"stockName":"USDINR","price":"67.3264","change":"+0.245","pChange":"+0.37"}],"recentNews":[{"img":"apps\AdvisorApp\common\images\news1.jpg","titel":"British woman MP shot, stabbed; in critical condition"},{"img":"apps\AdvisorApp\common\images\news2.jpg","titel":"Kamal Nath clarifies 'no one asked me to quit'; rivals want his role in 1984 riots investigated"},{"img":"apps\AdvisorApp\common\images\news3.jpg","titel":"HC directs internet providers and cable operators to take down illegal download links of Udta Punjab"},{"img":"apps\AdvisorApp\common\images\news4.jpg","titel":"Black Box Of Crashed Plane Found, Pulled Out Of Sea: Egypt"}]},"notification":{"message":["Customer Smith is redy to buy the product offered","Offer is rejected by Mohan","Anil has some query about the product","Please contact Rohan"]},"myLeads":[{"customerName":"Aman","convinced":"77","contactNo":"9667589330","timePeriod":"07"},{"customerName":"Mohan","convinced":"69","contactNo":"9667589330","timePeriod":"04"},{"customerName":"Nita","convinced":"30","contactNo":"9667589330","timePeriod":"08"},{"customerName":"Rohan","convinced":"21","contactNo":"9667589330","timePeriod":"02"}],"chatBot":{"customers":[{"customerName":"Ram","interest":"New Products","availability":"online"},{"customerName":"Mohan","interest":"Follow up","availability":"offline"},{"customerName":"Rohan","interest":"Customer history","availablity":"online"},{"customerName":"Gita","interest":"Leaderboard","availability":"offline"},{"customerName":"Smith","interest":"Lorem Ipsum","availability":"online"},{"customerName":"Hardi","interest":"Appoinments","availablity":"offline"}]},"agentDetail":{"agent":[{"agentName":"Ramjeet","contactNo":"9008986543","address":"#21,floor 3,2nd Cross,Jainagar","pin":"560123"}]}}],"isSuccessful":"true"},"isSuccessful":true}}
// },
{
	REQUEST:{"adapter":"AgentPerformanceService"},
	RESPONSE:{"status":200,"invocationContext":null,"invocationResult":{"response":{"responsesList":[{"pageData":{"overAllDetail":{"target":"1,50,000","earnedCommision":"75,000","startDate":"01/05/2016","endDate":"16/05/2016","memberType":"Silver","behindPer":"40"},"convert":[{"percent":"77","customerName":"Hardi","contactNo":"8776767676","product":"Metlife life time insurance"},{"percent":"52","customerName":"Manasi","contactNo":"8998976543","product":"Metlife life time insurance"},{"percent":"66","customerName":"Madona","contactNo":"8998976544","product":"Metlife insurance"}],"sell":[{"product":"Matlife longterm insurance","detail":"Financial security through regular income. Cover for spouse","status":"active"},{"product":"Matlife longterm insurance","detail":"Financial security through regular income. Cover for spouse","status":"inactive"},{"product":"Matlife longterm insurance","detail":"Financial security through regular income. Cover for spouse","status":"active"}],"meet":[{"percent":"77","customerName":"Gohan","contactNo":"9667589330","product":"Metlife life time insurance"},{"percent":"65","customerName":"Manasa","contactNo":"9667589330","product":"Metlife life time insurance"},{"percent":"54","customerName":"Tanui","contactNo":"9667589330","product":"Metlife life time insurance"}]},"notification":{"message":["Customer Smith is ready to buy the product offered","Offer is rejected by Mohan","Anil has some query about the product","Please contact Rohan"]},"myLeads":[{"customerName":"Aman","convinced":"77","contactNo":"9667589330","timePeriod":"07"},{"customerName":"Mohan","convinced":"69","contactNo":"9667589330","timePeriod":"04"},{"customerName":"Nita","convinced":"30","contactNo":"9667589330","timePeriod":"08"},{"customerName":"Rohan","convinced":"21","contactNo":"9667589330","timePeriod":"02"}],"chatBot":{"customers":[{"customerName":"Ram","interest":"New Products","availability":"online"},{"customerName":"Mohan","interest":"Follow up","availability":"offline"},{"customerName":"Rohan","interest":"Customer history","availability":"online"},{"customerName":"Gita","interest":"Leaderboard","availability":"offline"},{"customerName":"Smith","interest":"Lorem Ipsum","availability":"online"},{"customerName":"Hardi","interest":"Appointments","availability":"offline"}]},"agentDetail":{"agent":[{"agentName":"Ramjeet","contactNo":"9008986543","address":"#21,floor 3,2nd Cross,Jainagar","pin":"560123"}]}}],"isSuccessful":"true"},"isSuccessful":true}}
},
{
	REQUEST:{"adapter":"CustomerDetailService"},
	RESPONSE:{"status":200,"invocationContext":null,"invocationResult":{"response":{"responsesList":[{"pageData":{"customerDetail":{"customerName":"Rakshit Shah","gender":"Male","family":"Wife, 2 Children","work":"Design Studio","position":"Senior creative officer","education":"St. Joseph's College","degree":"Master degree","interest":"Sports, Child education, latest news about product","income":"1200000","dateDOB":"22,Jun","yearDOB":"1983","Address":"05, B wing, Sunrise Towers, Near Town Andheri West, Mumbai","distanceTime":"20 min","distance":"away from your current location","pin":"5600125","location":"Mumbai","country":"India"},"riskAppetite":{"debt":"20","equality":"45","longTermInvestment":"35"},"recommendation":{"annualInvestment":"120000","timePeriod":"10","children":[{"name":"Ranjeet","DOB":"26/09/1995"},{"name":"Gita","DOB":"03/01/1997"}]},"requirement":"Child Education Plan","topRecommendations":[{"plan":"Uchch Shiksha Plan","about":"90% People bought this policy","policyCover":"8000000","policyTerm":"10","img":"images/recommendation1.jpg","reviews":[{"customerName":"Rohan Raj","data":"This has been the best policy ever..."}]},{"plan":"Child Education Planner","about":"57% People bought this policy","policyCover":"8000000","policyTerm":"10","img":"images/recommendation2.jpg","reviews":[{"customerName":"Aman Kumar","data":"This has been the best policy ever..."}]},{"plan":"Surakshit Pariwar Plicy","about":"21% People bought this policy","policyCover":"8000000","policyTerm":"10","img":"images/pop-up-sm.jpg","reviews":[{"customerName":"Vishal Chhabria","data":"This has been the best policy ever..."}]}]},"notification":{"message":["Customer Smith is ready to buy the product offered","Offer is rejected by Mohan","Anil has some query about the product","Please contact Rohan"]},"myLeads":[{"customerName":"Aman","convinced":"77","contactNo":"9667589330","timePeriod":"07"},{"customerName":"Mohan","convinced":"69","contactNo":"9667589330","timePeriod":"04"},{"customerName":"Nita","convinced":"30","contactNo":"9667589330","timePeriod":"08"},{"customerName":"Rohan","convinced":"21","contactNo":"9667589330","timePeriod":"02"}],"chatBot":{"customers":[{"customerName":"Ram","interest":"New Products","availability":"online"},{"customerName":"Mohan","interest":"Follow up","availability":"offline"},{"customerName":"Rohan","interest":"Customer history","availability":"online"},{"customerName":"Gita","interest":"Leaderboard","availability":"offline"},{"customerName":"Smith","interest":"Lorem Ipsum","availability":"online"},{"customerName":"Hardi","interest":"Appointments","availability":"offline"}]},"agentDetail":{"agent":[{"agentName":"Ramjeet","contactNo":"9008986543","address":"#21,floor 3,2nd Cross,Jainagar","pin":"560123"}]}}],"isSuccessful":"true"},"isSuccessful":true}}
},
{
    REQUEST:{"adapter":"patronService"},
    RESPONSE:{"status":200,"invocationContext":null,"invocationResult":{"response":{"responsesList":[{
	"pageData": {
		"patronDetail": {
			"PatronNumber": "2345",
			"PatronName": "Vinay",
			"RatingTime": "546",
			"HoursPlayed": "4343",
		  	"PausedTime": "2",
			"AverageBetting": "5",
			"BettingFrequency": "546",
			"TurnoverAmount": "434"
		}
	}
}], "isSuccessful": "true"
}, "isSuccessful": true
}
}
},

{
    REQUEST: { "adapter": "adjustGamingTableOpenService" },
    RESPONSE: {
        "status": 200, "invocationContext": null, "invocationResult": {
            "response": {
                "responsesList": [{
                    "pageData": {
                        "adjustGamingTableOpenDetail": {
                            "ChipType": "2345",
                            "DenominationValue": "234",
                            "AmountForDenomination": "546",
                            "QuantityForDenomination": "43",
                            "GrandTotal": "28467"
                        }
                    }
                }], "isSuccessful": "true"
            }, "isSuccessful": true
        }
    }
},

{
    REQUEST:{"adapter":"paymentService"},
    RESPONSE:{"status":200,"invocationContext":null,"invocationResult":{"response":{"responsesList":[{"pageData":{"payment":{"name":"Shashank Patil","cardNumber":"5678-5432-2345-2345","cvv":"546","adhaar":"43435654324"}}}],"isSuccessful":"true"},"isSuccessful":true}}
},
{
    REQUEST:{"adapter":"AgentPerformanceAnalysisService"},
    RESPONSE:{"status":200,"invocationContext":null,"invocationResult":{"response":{"responsesList":[{"pageData":{"performanceAnalysis":{"perRatio":{"ulipPolicy":"70","termPolicy":"30","convert":"12"},"typicallySold":{"Average":"2"}},"nextLevel":"GOLD","recommendations":{"undertake":[{"training":"Improve effectiveness in sales","schedule":"12 Jun 2016"},{"training":"Daily sales training","schedule":"16 Jun 2016"},{"training":"Learn art of sales","schedule":"30 Jun 2016"},{"training":"Professional sales and marketing","schedule":"15 Jul 2016"}],"receive":[{"certification":"Associate in general insurance"},{"certification":"Chartered insurance professional"},{"certification":"Insurance licensing"}]}},"popUp":[{"product":"Uchch Shiksha Plan"},{"product":"Child Education Planner"},{"product":"Abroad Study Plan"}],"notification":{"message":["Customer Smith is ready to buy the product offered","Offer is rejected by Mohan","Anil has some query about the product","Please contact Rohan"]},"myLeads":[{"customerName":"Aman","convinced":"77","contactNo":"9667589330","timePeriod":"07"},{"customerName":"Mohan","convinced":"69","contactNo":"9667589330","timePeriod":"04"},{"customerName":"Nita","convinced":"30","contactNo":"9667589330","timePeriod":"08"},{"customerName":"Rohan","convinced":"21","contactNo":"9667589330","timePeriod":"02"}],"chatBot":{"customers":[{"customerName":"Ram","interest":"New Products","availability":"online"},{"customerName":"Mohan","interest":"Follow up","availability":"offline"},{"customerName":"Rohan","interest":"Customer history","availability":"online"},{"customerName":"Gita","interest":"Leaderboard","availability":"offline"},{"customerName":"Smith","interest":"Lorem Ipsum","availability":"online"},{"customerName":"Hardi","interest":"Appointments","availability":"offline"}]},"agentDetail":{"agent":[{"agentName":"Ramjeet","contactNo":"9008986543","address":"#21,floor 3,2nd Cross,Jainagar","pin":"560123"}]}}],"isSuccessful":"true"},"isSuccessful":true}}
},
{
    REQUEST:{"adapter":"postRatingSuccessService"},
    RESPONSE:{"status":200,"invocationContext":null,"invocationResult":{"response":{"responsesList":[{"pageData":{"customerDetail":{"customerName":"Shashank Patil","age":"33","profile":"Knowledge seeker","gender":"Male","phNumber":"8799766545","policyName":"Uchch Shiksha plan","tranNo":"12876","anualPremiuam":"12,000","policyTerm":"15 years","location":"Mumbai","country":"India"},"topRecommendations":[{"plan":"Uchch Shiksha Plan","about":"90% People bought this policy","policyCover":"8000000","policyTerm":"10","img":"images/recommendation1.jpg","reviews":[{"customerName":"Vishal Chhabria","data":"This has been the best policy ever..."}]},{"plan":"Lorem Ipsum","about":"57% People bought this policy","policyCover":"8000000","policyTerm":"10","img":"images/recommendation2.jpg","reviews":[{"customerName":"Vishal Chhabria","data":"This has been the best policy ever..."}]},{"plan":"Abroad Study Plan","about":"21% People bought this policy","policyCover":"8000000","policyTerm":"10","img":"images/recommendation3.jpg","reviews":[{"customerName":"Vishal Chhabria","data":"This has been the best policy ever..."}]}]},"notification":{"message":["Customer Smith is ready to buy the product offered","Offer is rejected by Mohan","Anil has some query about the product","Please contact Rohan"]},"myLeads":[{"customerName":"Aman","convinced":"77","contactNo":"9667589330","timePeriod":"07"},{"customerName":"Mohan","convinced":"69","contactNo":"9667589330","timePeriod":"04"},{"customerName":"Nita","convinced":"30","contactNo":"9667589330","timePeriod":"08"},{"customerName":"Rohan","convinced":"21","contactNo":"9667589330","timePeriod":"02"}],"chatBot":{"customers":[{"customerName":"Ram","interest":"New Products","availability":"online"},{"customerName":"Mohan","interest":"Follow up","availability":"offline"},{"customerName":"Rohan","interest":"Customer history","availability":"online"},{"customerName":"Gita","interest":"Leaderboard","availability":"offline"},{"customerName":"Smith","interest":"Lorem Ipsum","availability":"online"},{"customerName":"Hardi","interest":"Appointments","availability":"offline"}]},"agentDetail":{"agent":[{"agentName":"Ramjeet","contactNo":"9008986543","address":"#21,floor 3,2nd Cross,Jainagar","pin":"560123"}]}}],"isSuccessful":"true"},"isSuccessful":true}}
},
{
    REQUEST:{"adapter":"postRatingFormService"},
    RESPONSE:{"status":200,"invocationContext":null,"invocationResult":{"response":{"responsesList":[{"pageData":{"personalDetails":{"name":"Shashank Patil","parent":"Ramdas","maritalStatus":"Married","DOB":"20-12-1983","residenence":"K.R.Puram,Bangalore","nationality":"Indian","eMail":"shashank@gmail.com","panDetails":"CERSD4354Q","mobile":"+91-8725324356"},"educationEmployment":{"Qualification":"M.Tech","designation":"Manager","organisation":"Pwc","natureOfWork":"Consulting"},"nominee":{"nomineeName":"Ramdas","nomineeDob":"12-01-1958","address":"202,Shivaji Nagar,Bagalore","nomineeRelation":"Father"}},"notification":{"message":["Customer Smith is ready to buy the product offered","Offer is rejected by Mohan","Anil has some query about the product","Please contact Rohan"]},"myLeads":[{"customerName":"Aman","convinced":"77","contactNo":"9667589330","timePeriod":"07"},{"customerName":"Mohan","convinced":"69","contactNo":"9667589330","timePeriod":"04"},{"customerName":"Nita","convinced":"30","contactNo":"9667589330","timePeriod":"08"},{"customerName":"Rohan","convinced":"21","contactNo":"9667589330","timePeriod":"02"}],"chatBot":{"customers":[{"customerName":"Ram","interest":"New Products","availability":"online"},{"customerName":"Mohan","interest":"Follow up","availability":"offline"},{"customerName":"Rohan","interest":"Customer history","availability":"online"},{"customerName":"Gita","interest":"Leader board","availability":"offline"},{"customerName":"Smith","interest":"Lorem Ipsum","availability":"online"},{"customerName":"Hardi","interest":"Appointments","availability":"offline"}]},"agentDetail":{"agent":[{"agentName":"Ramjeet","contactNo":"9008986543","address":"#21,floor 3,2nd Cross,Jainagar","pin":"560123"}]}}],"isSuccessful":"true"},"isSuccessful":true}}
}
];


    return {
        getResponse: function(request) {
            for (var i = 0; i < messageList.length; i++) {
                if (request.adapter == messageList[i].REQUEST.adapter) {

                    if (request.adapter == "JSONLocationService") {
                        if (request.parameters[0].indexOf("searchString") > -1 && _.isUndefined(messageList[i].REQUEST.parameters[0].searchString)) {
                            continue;
                        } 
                    }
                    // for refreshing the auth value;
                    if (messageList[i].RESPONSE.hasOwnProperty('responsesList')){
	                    if (messageList[i].RESPONSE.responsesList[0].hasOwnProperty('auth')) {
	                    	messageList[i].RESPONSE.responsesList[0].auth ='Transaction Password';
	                    }
                    }
                    return messageList[i].RESPONSE;
                }

            }

            alert("No stubs found");
        }
    }

}]);
