$.ajax({cache:false});
/* JavaScript content from js/main.js in folder common */
var App={};
App.viewModels={};

var testApp = angular.module('testApp', ['ngRoute','AppController','formly', 'formlyBootstrap']);

//Define a module for controllers and services
var AppController = angular.module("AppController", []);

//Config for the mobile app
testApp.config(['$routeProvider', function($routeProvider) {
	$routeProvider.
	when('/navigation/:feature/resources/:page', {
		templateUrl: function (parameters) {
			// Here, we dynamically create the url for the template
			// The url of the page is just a .html added to
			// the url.
			return 'navigation/' + parameters.feature + '/resources/' +
			parameters.page + '.html';
		},
		controller: 'AppController'
	});
}]);


