module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    copy: {
      files: {
        cwd: 'apps/AdvisorApp/common',  // set working folder / root to copy
        src: '**/*',           // copy all files and subfolders
        dest: '../metlifeapp/www/',    // destination folder
        expand: true           // required when using cwd
  }
}
  });

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-copy');
  // Default task(s).
  grunt.registerTask('default', ['copy']);

};